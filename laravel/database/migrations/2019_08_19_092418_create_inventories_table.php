<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name',150)->unique();
            $table->string('address')->nullable();
            $table->integer('total')->default(0)->nullable();
            $table->integer('max')->default(0)->nullable();
            $table->integer('the_rest')->default(0)->nullable();
            $table->tinyInteger('status')->default(0)->comment('0: Open, 1: Closed');
            $table->timestamps();
        });
        Schema::create('user_inventory', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->bigInteger('inventory_id')->unsigned();
            $table->foreign('inventory_id')->references('id')->on('inventories')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventories');
        Schema::dropIfExists('user_inventory');
    }
}
