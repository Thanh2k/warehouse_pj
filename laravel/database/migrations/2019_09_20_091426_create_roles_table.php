<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name',150)->unique();
            $table->string('description')->nullable();
            $table->timestamps();
        });

        DB::table('roles')->insert(
            array([
                'name' => 'Super admin',
                'description' => 'super admin'
            ],[
                'name' => 'Admin',
                'description' => 'admin'
            ],[
                'name' => 'Inventory manager',
                'description' => 'inventory manager'
            ] )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
}
