<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPhoneAddressStatusInUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('image')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('address')->nullable();
            $table->date('birthday')->nullable();
            $table->tinyInteger('first_login_status')->default(0)->comment('0: Not Change, 1: Password changed');
            $table->tinyInteger('level')->default(1)->comment('1: Admin, 2: Inventory manager');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('image');
            $table->dropColumn('phone_number');
            $table->dropColumn('address');
            $table->dropColumn('birthday');
            $table->dropColumn('first_login_status');
            $table->dropColumn('level');
        });
    }
}
