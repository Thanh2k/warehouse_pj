<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventory_product extends Model
{
    protected $table = 'inventory_product';

    protected $fillable = [
        'inventory_id', 'product_id', 'real_quantity', 'created_at','updated_at','newest_price_import'
    ];
}
