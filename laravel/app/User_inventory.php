<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_inventory extends Model
{
    protected $table = 'user_inventory';

    protected $fillable = [
        'user_id', 'inventory_id','created_at','updated_at'
    ];
}
