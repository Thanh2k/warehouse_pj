<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Inventory_product;
use App\Product;
use App\Category;
use App\Supplier;
use App\Inventory;
use App\Import;
use App\Unit;
use DB;
use Auth;
use Session;

class ImportImport implements ToCollection
{
    protected $invenid;

    public function __construct($invenid)
    {
        $this->invenid = $invenid; 
    }
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function collection(Collection $rows)
    {
        $rowSize = sizeof($rows);
        $categories = Category::pluck('name');
        $listCat = [];
        $suppliers = Supplier::pluck('name');
        $listSup = [];
        $units = Unit::pluck('name');
        $listUnit = [];
        $sumQuantity = 0;
        $inventory = Inventory::find($this->invenid);
        if ($rows[0][0] != 'Import Product') {
            Session()->flash('error', 'Please select the correct file template to insert new data !!!');
            return redirect()->back();
        }
        if ($rowSize <= 3) {
            Session()->flash('error', 'Please enter the information before import data !!!');
            return redirect()->back();
        }
        for ($i=3; $i < $rowSize; $i++) { 
            for ($j=0; $j < 7; $j++) { 
                if ($rows[$i][$j] = null) {
                    Session()->flash('error', 'Row '.($i+1).', column '.($j+1).' is not filled !!!');
                    return redirect()->back();
                }
            }
        }
        for ($i=3; $i < $rowSize; $i++) { 
            if($rows[$i][5] < 1){
                Session()->flash('error', 'Row '.($i+1).', column 6 , quantity product have to greater 1 !!!');
                return redirect()->back();
            }
            if($rows[$i][5] > 5000){
                Session()->flash('error', 'Row '.($i+1).', column 6 , quantity product have to less than 5000 !!!');
                return redirect()->back();
            }
            if($rows[$i][6] > 100000000){
                Session()->flash('error', 'Row '.($i+1).', column 7 , price product have to less than 100000000 !!!');
                return redirect()->back();
            }
            if($rows[$i][6] < 1){
                Session()->flash('error', 'Row '.($i+1).', column 7 , price product have to greater than 1 !!!');
                return redirect()->back();
            }
            $sumQuantity += $rows[$i][5];
        }

        if($sumQuantity > $inventory->max || $sumQuantity > $inventory->the_rest){
            Session()->flash('error', 'Inventory overflow . Total quantity product is '.$sumQuantity.'  But maximum capacity of this repository is '.$inventory->max.', the rest capacity of this repository is '.$inventory->the_rest.' !!!');
            return redirect()->back();
        }

        for ($i=3; $i < $rowSize; $i++) { 
            $category_exist_id = DB::table('categories')->select('id')->where('name','=',$rows[$i][2])->first();
            if (!$categories->contains($rows[$i][2]) && !$this->isExist($listCat,$rows[$i][2])) {
                array_push($listCat, $rows[$i][2]);
                $cat = Category::create([
                    'name' => $rows[$i][2],
                    'parent_id' => 0,
                ]);
            }

            $unit_exist_id = DB::table('units')->select('id')->where('name','=',$rows[$i][3])->first();
            if (!$units->contains($rows[$i][3]) && !$this->isExist($listUnit,$rows[$i][3])) {
                array_push($listUnit, $rows[$i][3]);
                $unit = Unit::create([
                    'name' => $rows[$i][3],
                ]);
            }

            $supplier_exist_id = DB::table('suppliers')->select('id')->where('name','=',$rows[$i][4])->first();
            if (!($suppliers->contains($rows[$i][4])) && ! $this->isExist($listSup,$rows[$i][4])) {
                array_push($listSup, $rows[$i][4]);
                $sup = Supplier::create([
                    'name' => $rows[$i][4],
                ]);
            }

            $proExist = DB::table('inventory_product')
                        ->join('products','products.id','=','inventory_product.product_id')
                        ->where([['inventory_product.inventory_id', '=', $this->invenid],['products.name','=',$rows[$i][1]]])
                        ->first();
            if (empty($proExist)) {
                $pro = Product::create([
                    'name' => $rows[$i][1],
                    'category_id' => ($category_exist_id != NULL) ? $category_exist_id->id : $cat->id,
                    'unit_id' => ($unit_exist_id != NULL) ? $unit_exist_id->id : $unit->id,
                    'created_at' => Carbon::now()->format('Y-m-d'),
                    'updated_at' => Carbon::now()->format('Y-m-d'),
                ]);
            }
            
            $import = Import::create([
                        'inventory_id' => $this->invenid,
                        'product_id' => ($proExist != NULL) ? $proExist->id : $pro->id,
                        'supplier_id' => ($supplier_exist_id != NULL) ? $supplier_exist_id->id : $sup->id,
                        'user_id' => Auth::id(),
                        'quantity_import' => $rows[$i][5],
                        'price_import' => $rows[$i][6],
                        'created_at' => Carbon::now()->format('Y-m-d'),
                        'updated_at' => Carbon::now()->format('Y-m-d'),
                    ]);
            $invenProductExist = Inventory_product::where([
                ['inventory_id','=',$this->invenid],
                ['product_id','=',($proExist != NULL) ? $proExist->id : $pro->id],
            ])->first();
            if (!empty($invenProductExist)) {
                $realQuantity = ($invenProductExist->real_quantity) + ($rows[$i][5]);
                $invenProduct = DB::table('inventory_product')
                                ->where([
                                    ['inventory_id','=',$this->invenid],
                                    ['product_id','=',($proExist != NULL) ? $proExist->id : $pro->id],
                                ])
                                ->update([
                                    'real_quantity' => $realQuantity,
                                    'newest_price_import' => $rows[$i][6],
                                ]);
            }else{
                $invenProduct = Inventory_product::create([
                            'inventory_id' => $this->invenid,
                            'product_id' => ($proExist != NULL) ? $proExist->id : $pro->id,
                            'real_quantity' => $rows[$i][5],
                            'newest_price_import' => $rows[$i][6],
                            'created_at' => Carbon::now()->format('Y-m-d'),
                            'updated_at' => Carbon::now()->format('Y-m-d'),
                        ]);
            }
            $totalProduct = DB::table('inventory_product')->where('inventory_id',$this->invenid)->sum('real_quantity');
            $inventory->total = $totalProduct;
            $inventory->the_rest = $inventory->the_rest - $rows[$i][5];
            $inventory->save();
        }
        Session()->flash('success', 'Import data successfully !!!');
        return redirect()->back();
    }

    public function isExist($arr, $el){
        $len = sizeof($arr);
        for ($i=0; $i < $len; $i++) { 
            if($arr[$i] == $el){
                return true;
            }
        }
        return false;
    }
}
