<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use App\Role;
use App\Role_user;
use App\User;
use Auth;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        \App\User::class => \App\Policies\AdminPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        //Khởi tạo các Gate mới
        if (! $this->app->runningInConsole()) {
            foreach (Role::all() as $roles) {
                Gate::define($roles->name,function ($user) use ($roles)
                {
                    return $user->hasRoles($roles);
                });
            }
        }
    }
}
