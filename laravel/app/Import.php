<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Import extends Model
{
    protected $table = 'import';

    protected $fillable = [
        'user_id', 'inventory_id', 'product_id', 'supplier_id', 'quantity_import', 'price_import', 'created_at','updated_at','type'
    ];

    public function scopeKey($query, $request)
	{
		$invenid = $request->invenid;
	    if ($request->has('key') && $request->key != "new" && $request->key != "returned") {
            $query->join('products', 'products.id', '=', 'import.product_id')
            ->join('suppliers', 'suppliers.id', '=', 'import.supplier_id')
            ->crossJoin('users', 'users.id', '=', 'import.user_id')
            ->where([
                ['import.inventory_id', '=', $invenid],
                ['import.quantity_import', '=', $request->key],
            ])
            ->orWhere([
                ['import.inventory_id', '=', $invenid],
                ['import.price_import', '=', $request->key],
            ])
            ->orWhere([
                ['import.inventory_id', '=', $invenid],
                ['products.name', 'LIKE', '%' . $request->key . '%'],
            ])
            ->orWhere([
                ['import.inventory_id', '=', $invenid],
                ['suppliers.name', 'LIKE', '%' . $request->key . '%'],
            ])
            ->orWhere([
                ['import.inventory_id', '=', $invenid],
                ['users.username', 'LIKE', '%' . $request->key . '%'],
            ])
            ->select('import.*','products.name','users.username as username','suppliers.name as supname')
            ->orderBy('import.id', 'desc');
        }else if($request->key == "new" || $request->key == "returned"){
            $query->join('products', 'products.id', '=', 'import.product_id')
            ->join('suppliers', 'suppliers.id', '=', 'import.supplier_id')
            ->crossJoin('users', 'users.id', '=', 'import.user_id')
            ->where([
                ['import.inventory_id', '=', $invenid],
                ['import.type', '=', ($request->key == 'new' ? '0' : '1')],
            ])
            ->select('import.*','products.name','users.username as username','suppliers.name as supname')
            ->orderBy('import.id', 'desc');
        }

	    return $query;
	}

	public function scopeFrom($query, $request)
	{
		$invenid = $request->invenid;
	    if ($request->from != null && $request->to == null) {
            $query->join('products', 'products.id', '=', 'import.product_id')
            ->join('suppliers', 'suppliers.id', '=', 'import.supplier_id')
            ->crossJoin('users', 'users.id', '=', 'import.user_id')
            ->where([
                ['import.inventory_id', '=', $invenid],
                ['import.created_at', '=', date('Y-m-d',strtotime($request->from))],
            ])
            ->select('import.*','products.name','users.username as username','suppliers.name as supname')
            ->orderBy('import.id', 'desc');
        }

	    return $query;
	}

	public function scopeTo($query, $request)
	{
		$invenid = $request->invenid;
	    if ($request->to != null && $request->from == null) {
            $query->join('products', 'products.id', '=', 'import.product_id')
            ->join('suppliers', 'suppliers.id', '=', 'import.supplier_id')
            ->crossJoin('users', 'users.id', '=', 'import.user_id')
            ->where([
                ['import.inventory_id', '=', $invenid],
                ['import.created_at', '=', date('Y-m-d',strtotime($request->to))],
            ])
            ->select('import.*','products.name','users.username as username','suppliers.name as supname')
            ->orderBy('import.id', 'desc');
        } 

	    return $query;
	}

	public function scopeFromTo($query, $request)
	{
		$invenid = $request->invenid;
	    if ($request->from != null && $request->to != null) {
            $query->join('products', 'products.id', '=', 'import.product_id')
                ->join('suppliers', 'suppliers.id', '=', 'import.supplier_id')
                ->crossJoin('users', 'users.id', '=', 'import.user_id')
                ->where([
                    ['import.inventory_id', '=', $invenid],
                    ['import.created_at', '>=', date('Y-m-d',strtotime($request->from))],
                    ['import.created_at', '<=', date('Y-m-d',strtotime($request->to))],
                ])
                ->select('import.*','products.name','users.username as username','suppliers.name as supname')
                ->orderBy('import.id', 'desc');
        }

	    return $query;
	}
}
