<?php

namespace App\Exports;

use App\Product;
use DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Carbon\Carbon;

class ExcelProduct implements FromView
{
	use Exportable;

	protected $invenid;
    public function __construct($invenid)
    {
        $this->invenid = $invenid; 
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        $date = Carbon::now()->toFormattedDateString();
        $inventory = DB::table('inventories')->where('id',$this->invenid)->first();
        $products =  DB::table('products')
                ->join('inventory_product', 'inventory_product.product_id', '=', 'products.id')
                ->join('categories', 'categories.id', '=', 'products.category_id')
                ->join('units', 'units.id', '=', 'products.unit_id')
                ->join('import', 'import.product_id', '=', 'products.id')
                ->crossJoin('suppliers', 'suppliers.id', '=', 'import.supplier_id')
                ->where('inventory_product.inventory_id', '=', $this->invenid)
                ->select('inventory_product.*','products.*','categories.name as catname','units.name as unitname','suppliers.name as supname')
                ->groupBy('products.id','inventory_product.id')
                ->orderBy('products.id','DESC')
                ->get();
        return view('exports.productexcel',compact(['products','inventory','date']));
    }

}
