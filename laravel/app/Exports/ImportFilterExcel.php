<?php

namespace App\Exports;

use App\Import;
use DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Carbon\Carbon;

class ImportFilterExcel implements FromView
{
	use Exportable;

    protected $invenid;
    protected $key;
    protected $from;
    protected $to;
    public function __construct($invenid, $key, $from, $to)
    {
        $this->invenid = $invenid; 
        $this->key = $key; 
        $this->from = $from; 
        $this->to = $to; 
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        $date = Carbon::now()->toFormattedDateString();

        if ($this->key != 'z' && $this->key != "new" && $this->key != "returned") {
            $inventory = DB::table('inventories')->where('id',$this->invenid)->first();
            $import = Import::query()
                ->join('products', 'products.id', '=', 'import.product_id')
                ->join('suppliers', 'suppliers.id', '=', 'import.supplier_id')
                ->crossJoin('users', 'users.id', '=', 'import.user_id')
                ->where([
                    ['import.inventory_id', '=', $this->invenid],
                    ['import.quantity_import', '=', $this->key],
                ])
                ->orWhere([
                    ['import.inventory_id', '=', $this->invenid],
                    ['import.price_import', '=', $this->key],
                ])
                ->orWhere([
                    ['import.inventory_id', '=', $this->invenid],
                    ['products.name', 'LIKE', '%' . $this->key . '%'],
                ])
                ->orWhere([
                    ['import.inventory_id', '=', $this->invenid],
                    ['suppliers.name', 'LIKE', '%' . $this->key . '%'],
                ])
                ->orWhere([
                    ['import.inventory_id', '=', $this->invenid],
                    ['users.username', 'LIKE', '%' . $this->key . '%'],
                ])
                ->select('import.*','products.name as proname','users.username as username','suppliers.name as supname')->get();
        return view('exports.filterimportexcel',compact(['import','inventory','date']));
        }else if($this->key != 'z' && ($this->key == "new" || $this->key == "returned")){
            $inventory = DB::table('inventories')->where('id',$this->invenid)->first();
            $import = Import::query()
                ->join('products', 'products.id', '=', 'import.product_id')
                ->join('suppliers', 'suppliers.id', '=', 'import.supplier_id')
                ->crossJoin('users', 'users.id', '=', 'import.user_id')
                ->where([
                    ['import.inventory_id', '=', $this->invenid],
                    ['import.type', '=', ($this->key == 'new' ? '0' : '1')],
                ])
                ->select('import.*','products.name as proname','users.username as username','suppliers.name as supname')->get();
        return view('exports.filterimportexcel',compact(['import','inventory','date']));
        }else if ($this->from != 'z' && $this->to == 'z') {
            $inventory = DB::table('inventories')->where('id',$this->invenid)->first();
            $import = Import::query()
                ->join('products', 'products.id', '=', 'import.product_id')
                ->join('suppliers', 'suppliers.id', '=', 'import.supplier_id')
                ->crossJoin('users', 'users.id', '=', 'import.user_id')
                ->where([
                    ['import.inventory_id', '=', $this->invenid],
                    ['import.created_at', '=', date('Y-m-d',strtotime($this->from))],
                ])
                ->select('import.*','products.name as proname','users.username as username','suppliers.name as supname')->get();
        return view('exports.filterimportexcel',compact(['import','inventory','date']));
        }else if ($this->from == 'z' && $this->to != 'z') {
            $inventory = DB::table('inventories')->where('id',$this->invenid)->first();
            $import = Import::query()
                ->join('products', 'products.id', '=', 'import.product_id')
                ->join('suppliers', 'suppliers.id', '=', 'import.supplier_id')
                ->crossJoin('users', 'users.id', '=', 'import.user_id')
                ->where([
                    ['import.inventory_id', '=', $this->invenid],
                    ['import.created_at', '=', date('Y-m-d',strtotime($this->to))],
                ])
                ->select('import.*','products.name as proname','users.username as username','suppliers.name as supname')->get();
        return view('exports.filterimportexcel',compact(['import','inventory','date']));
        }else if ($this->from != 'z' && $this->to != 'z') {
            $inventory = DB::table('inventories')->where('id',$this->invenid)->first();
            $import = Import::query()
                ->join('products', 'products.id', '=', 'import.product_id')
                ->join('suppliers', 'suppliers.id', '=', 'import.supplier_id')
                ->crossJoin('users', 'users.id', '=', 'import.user_id')
                ->where([
                    ['import.inventory_id', '=', $this->invenid],
                    ['import.created_at', '>=', date('Y-m-d',strtotime($this->from))],
                    ['import.created_at', '<=', date('Y-m-d',strtotime($this->to))],
                ])
                ->select('import.*','products.name as proname','users.username as username','suppliers.name as supname')->get();
        return view('exports.filterimportexcel',compact(['import','inventory','date']));
        }
    }
}
