<?php

namespace App\Exports;

use App\Import;
use DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Carbon\Carbon;

class ImportExcel implements FromView
{

    use Exportable;

    protected $invenid;
    public function __construct($invenid)
    {
        $this->invenid = $invenid; 
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        $date = Carbon::now()->toFormattedDateString();
        $inventory = DB::table('inventories')->where('id',$this->invenid)->first();
        $import = DB::table('import')
                ->join('products', 'products.id', '=', 'import.product_id')
                ->join('suppliers', 'suppliers.id', '=', 'import.supplier_id')
                ->crossJoin('users', 'users.id', '=', 'import.user_id')
                ->where('import.inventory_id', '=', $this->invenid)
                ->select('products.name as proname','users.username as username','import.quantity_import','import.price_import','import.price_product_return','import.type','import.created_at','suppliers.name as supname')
                ->get();
        return view('exports.importexcel',compact(['import','inventory','date']));
    }
}
