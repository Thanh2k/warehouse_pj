<?php

namespace App\Exports;

use App\Export;
use DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Carbon\Carbon;

class ExportFilterExcel implements FromView
{
	use Exportable;

    protected $invenid;
    protected $key;
    protected $from;
    protected $to;
    public function __construct($invenid,$key,$from,$to)
    {
        $this->invenid = $invenid; 
        $this->key = $key; 
        $this->from = $from; 
        $this->to = $to; 
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        $date = Carbon::now()->toFormattedDateString();

        if ($this->key != 'z') {
            $inventory = DB::table('inventories')->where('id',$this->invenid)->first();
            $export = Export::query()
                ->join('products', 'products.id', '=', 'export.product_id')
                ->join('customers', 'customers.id', '=', 'export.customer_id')
                ->crossJoin('users', 'users.id', '=', 'export.user_id')
                ->where([
                    ['export.inventory_id', '=', $this->invenid],
                    ['export.quantity_export', '=', $this->key],
                ])
                ->orWhere([
                    ['export.inventory_id', '=', $this->invenid],
                    ['export.price_export', '=', $this->key],
                ])
                ->orWhere([
                    ['export.inventory_id', '=', $this->invenid],
                    ['products.name', 'LIKE', '%' . $this->key . '%'],
                ])
                ->orWhere([
                    ['export.inventory_id', '=', $this->invenid],
                    ['users.username', 'LIKE', '%' . $this->key . '%'],
                ])
                ->orWhere([
                    ['export.inventory_id', '=', $this->invenid],
                    ['customers.name', 'LIKE', '%' . $this->key . '%'],
                ])
                ->select('export.*','products.name as proname','customers.name as cusname','users.username as username')->get();
        return view('exports.filterexportexcel',compact(['export','inventory','date']));
        }else if ($this->from != 'z' && $this->to == 'z') {
            $inventory = DB::table('inventories')->where('id',$this->invenid)->first();
            $export = Export::query()
                ->join('products', 'products.id', '=', 'export.product_id')
                ->join('customers', 'customers.id', '=', 'export.customer_id')
                ->crossJoin('users', 'users.id', '=', 'export.user_id')
                ->where([
                    ['export.inventory_id', '=', $this->invenid],
                    ['export.created_at', '=', date('Y-m-d',strtotime($this->from))],
                ])
                ->select('export.*','products.name as proname','customers.name as cusname','users.username as username')->get();
        return view('exports.filterexportexcel',compact(['export','inventory','date']));
        }else if ($this->from == 'z' && $this->to != 'z') {
            $inventory = DB::table('inventories')->where('id',$this->invenid)->first();
            $export = Export::query()
                ->join('products', 'products.id', '=', 'export.product_id')
                ->join('customers', 'customers.id', '=', 'export.customer_id')
                ->crossJoin('users', 'users.id', '=', 'export.user_id')
                ->where([
                    ['export.inventory_id', '=', $this->invenid],
                    ['export.created_at', '=', date('Y-m-d',strtotime($this->to))],
                ])
                ->select('export.*','products.name as proname','customers.name as cusname','users.username as username')->get();
        return view('exports.filterexportexcel',compact(['export','inventory','date']));
        }else if ($this->from != 'z' && $this->to != 'z') {
            $inventory = DB::table('inventories')->where('id',$this->invenid)->first();
            $export = Export::query()
                ->join('products', 'products.id', '=', 'export.product_id')
                ->join('customers', 'customers.id', '=', 'export.customer_id')
                ->crossJoin('users', 'users.id', '=', 'export.user_id')
                ->where([
                    ['export.inventory_id', '=', $this->invenid],
                    ['export.created_at', '>=', date('Y-m-d',strtotime($this->from))],
                    ['export.created_at', '<=', date('Y-m-d',strtotime($this->to))],
                ])
                ->select('export.*','products.name as proname','customers.name as cusname','users.username as username')->get();
        return view('exports.filterexportexcel',compact(['export','inventory','date']));
        }
    }

}
