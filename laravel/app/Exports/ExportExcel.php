<?php

namespace App\Exports;

use App\Export;
use DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Carbon\Carbon;

class ExportExcel implements FromView
{
	use Exportable;

	protected $invenid;
    public function __construct($invenid)
    {
        $this->invenid = $invenid; 
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        $date = Carbon::now()->toFormattedDateString();
        $inventory = DB::table('inventories')->where('id',$this->invenid)->first();
        $export = DB::table('export')
                ->join('products', 'products.id', '=', 'export.product_id')
                ->join('customers', 'customers.id', '=', 'export.customer_id')
                ->crossJoin('users', 'users.id', '=', 'export.user_id')
                ->where('inventory_id', '=', $this->invenid)
                ->select('products.name as proname','customers.name as cusname','users.username as username','export.quantity_export','export.price_export','export.created_at')
                ->get();
        return view('exports.exportexcel',compact(['export','inventory','date']));
    }
}
