<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestImportPlusProduct extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'quantity_import'=>'required|numeric|integer|max:5000|min:1',
            'price_import'=>'required|numeric|max:100000000|min:1',
        ];
    }
}
