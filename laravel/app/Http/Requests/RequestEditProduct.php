<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestEditProduct extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|min:3|max:150',
            'category_id'=>'required',
            'supplier_id'=>'required',
            'unit_id'=>'required',
            'image'=>'mimes:jpeg,jpg,png,gif|max:10000',
        ];
    }
}
