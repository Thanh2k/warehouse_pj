<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use DB;
use App\Role;
use App\User;

class CheckAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$roles = null)
    {
        $listRoleOfUser = User::find(Auth::id())->role()->select('roles.id')->pluck('id')->unique();
        //lay ra ma man hinh tuong ung de check phan quyen
        $checkRole = Role::where('name',$roles)->value('id');
        //check user co dc vao man hinh hay ko
        if ($listRoleOfUser->contains($checkRole) || Auth::id() == 1) {
            return $next($request);
        }else{
            return abort(403);
        }

    }
}
