<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CheckFirstLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        if ($user->first_login_status == 0) {
            return redirect()->route('changeinfo');
        }else{
            return $next($request);
        }
    }
}
