<?php 
class Helpers{

	//Hàm hiển thị danh mục cho view add
	public static function showCategoriesForAddView($categories, $parent_id = 0, $char = '')
	{
		foreach ($categories as $key => $item)
		{
			if ($item->parent_id == $parent_id)
			{
				if(old('category_id') == $item->id){
					echo '<option value="'.$item->id.'" selected>'.$char.$item->name.'</option>';
				}else{
					echo '<option value="'.$item->id.'">'.$char.$item->name.'</option>';
				}
				unset($categories[$key]);

				Helpers::showCategoriesForAddView($categories, $item->id, $char.'-- ');
			}
		}
	}

	//Hàm hiển thị danh mục cho view edit
	public static function showCategoriesForEditView($categories, $category, $parent_id = 0, $char = '')
	{
		foreach ($categories as $key => $item)
		{
			if ($item->parent_id == $parent_id)
			{
				//Để xóa chính cái selected
				if($item->id != $category->id) {
					if($item->id == $category->parent_id){
						echo '<option value="'.$item->id.'" selected>'.$char.$item->name.'</option>';
					} else {
						echo '<option value="'.$item->id.'">'.$char.$item->name.'</option>';
					}
				}
				unset($categories[$key]);
				Helpers::showCategoriesForEditView($categories, $category, $item->id, $char.'-- ');
			}
		}
	}

	//Hàm hiển thị danh mục cho view edit product
	public static function showCategoriesForEditProduct($categories, $id, $parent_id = 0, $char = '')
	{
		foreach ($categories as $key => $item)
		{
			if ($item->parent_id == $parent_id)
			{
				if($item->id == $id) {
					echo '<option value="'.$item->id.'" selected>'.$char.$item->name.'</option>';
				} else {
					echo '<option value="'.$item->id.'">'.$char.$item->name.'</option>';
				}
				unset($categories[$key]);
				Helpers::showCategoriesForEditProduct($categories, $id, $item->id, $char.'-- ');
			}
		}
	}

	//Hàm xóa các danh mục con của danh mục được chọn để edit
	public static function clean(&$categories, $id)
	{
		foreach ($categories as $key => $item)
		{
			if ($item->parent_id == $id)
			{
	            // Xóa 
				unset($categories[$key]);
			}
		}
	}

	//Hàm hiển thị danh mục cho view index
	public static function showCategoriesForIndexView($categories, $parent_id = 0, $char = '')
	{
		foreach ($categories as $key => $item)
		{
			if ($item->parent_id == $parent_id)
			{
				echo '<tr>';
				echo '<td>'.$item->id.'</td>';
				echo '<td>'.$char . $item->name.'</td>';
				echo '<td>';
				echo '<a href="'.route('categories_edit',['id'=>$item->id]).'" class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-edit"></span></a>
					<a href="'.route('categories_delete',['id'=>$item->id]).'" onclick="return confirm(\'If you delete this category, you may lose the system history. Are you sure ?\')" class="btn btn-sm btn-danger"><span class="glyphicon glyphicon-trash"></span></a>';
				echo '</td>';
				echo '</tr>';

				unset($categories[$key]);

				Helpers::showCategoriesForIndexView($categories, $item->id, $char.'-- ');
			}
		}
	}
}

?>