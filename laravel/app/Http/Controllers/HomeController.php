<?php

namespace App\Http\Controllers;

use App\Charts\ImportExportChart;
use Illuminate\Http\Request;
use App\Inventory;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $countInven = DB::table('inventories')->count();
        $countCus = DB::table('customers')->count();
        $countSup = DB::table('suppliers')->count();
        $countManager = DB::table('users')
                        ->where('id', '<>', 1)
                        ->count();
        for ($i=1; $i <= 12; $i++) { 
            $export[$i] = DB::table('export')
            ->where([[DB::raw("DATE_FORMAT(created_at,'%Y')"),date('Y')],[DB::raw("DATE_FORMAT(created_at,'%m')"),$i]])
            ->select(DB::raw('SUM(quantity_export) AS sumexport'))
            ->get();
        }
        for ($i=1; $i <= 12; $i++) { 
            $import[$i] = DB::table('import')
            ->where([[DB::raw("DATE_FORMAT(created_at,'%Y')"),date('Y')],[DB::raw("DATE_FORMAT(created_at,'%m')"),$i]])
            ->select(DB::raw('SUM(quantity_import) AS sumimport'))
            ->get();
        }

        $chart = new ImportExportChart;
        $chart->labels(['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC']);
        $chart->title("Import and Export Chart");
        $chart->dataset('Quantity export', 'bar', [$export[1][0]->sumexport,$export[2][0]->sumexport,$export[3][0]->sumexport,$export[4][0]->sumexport,$export[5][0]->sumexport,$export[6][0]->sumexport,$export[7][0]->sumexport,$export[8][0]->sumexport,$export[9][0]->sumexport,$export[10][0]->sumexport,$export[11][0]->sumexport,$export[12][0]->sumexport])
                ->backgroundcolor("#c52828");
        $chart->dataset('Quantity import', 'bar', [$import[1][0]->sumimport,$import[2][0]->sumimport,$import[3][0]->sumimport,$import[4][0]->sumimport,$import[5][0]->sumimport,$import[6][0]->sumimport,$import[7][0]->sumimport,$import[8][0]->sumimport,$import[9][0]->sumimport,$import[10][0]->sumimport,$import[11][0]->sumimport,$import[12][0]->sumimport])
                ->backgroundcolor("#307fad");
        $chart->dataset('Ratio', 'bar', [$export[1][0]->sumexport - $import[1][0]->sumimport,$export[2][0]->sumexport - $import[2][0]->sumimport,$export[3][0]->sumexport - $import[3][0]->sumimport,$export[4][0]->sumexport - $import[4][0]->sumimport,$export[5][0]->sumexport - $import[5][0]->sumimport,$export[6][0]->sumexport - $import[6][0]->sumimport,$export[7][0]->sumexport - $import[7][0]->sumimport,$export[8][0]->sumexport - $import[8][0]->sumimport,$export[9][0]->sumexport - $import[9][0]->sumimport,$export[10][0]->sumexport - $import[10][0]->sumimport,$export[11][0]->sumexport - $import[11][0]->sumimport,$export[12][0]->sumexport - $import[12][0]->sumimport])
                ->backgroundcolor("orange");
        return view('home',compact(['countInven','countSup','countCus','countManager','chart']));
    }

    public function getFilter(Request $request)
    {
        $countInven = DB::table('inventories')->count();
        $countCus = DB::table('customers')->count();
        $countSup = DB::table('suppliers')->count();
        $countManager = DB::table('users')
                        ->where('id', '<>', 1)
                        ->count();
        for ($i=1; $i <= 12; $i++) { 
            $export[$i] = DB::table('export')
            ->where([[DB::raw("DATE_FORMAT(created_at,$request->key)"),date('Y')],[DB::raw("DATE_FORMAT(created_at,'%m')"),$i]])
            ->select(DB::raw('SUM(quantity_export) AS sumexport'))
            ->get();
        }
        for ($i=1; $i <= 12; $i++) { 
            $import[$i] = DB::table('import')
            ->where([[DB::raw("DATE_FORMAT(created_at,$request->key)"),date('Y')],[DB::raw("DATE_FORMAT(created_at,'%m')"),$i]])
            ->select(DB::raw('SUM(quantity_import) AS sumimport'))
            ->get();
        }

        $chart = new ImportExportChart;
        $chart->labels(['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC']);
        $chart->title("Import and Export Chart");
        $chart->dataset('Quantity export', 'bar', [$export[1][0]->sumexport,$export[2][0]->sumexport,$export[3][0]->sumexport,$export[4][0]->sumexport,$export[5][0]->sumexport,$export[6][0]->sumexport,$export[7][0]->sumexport,$export[8][0]->sumexport,$export[9][0]->sumexport,$export[10][0]->sumexport,$export[11][0]->sumexport,$export[12][0]->sumexport])
                ->backgroundcolor("#c52828");
        $chart->dataset('Quantity import', 'bar', [$import[1][0]->sumimport,$import[2][0]->sumimport,$import[3][0]->sumimport,$import[4][0]->sumimport,$import[5][0]->sumimport,$import[6][0]->sumimport,$import[7][0]->sumimport,$import[8][0]->sumimport,$import[9][0]->sumimport,$import[10][0]->sumimport,$import[11][0]->sumimport,$import[12][0]->sumimport])
                ->backgroundcolor("#307fad");
        $chart->dataset('Ratio', 'bar', [$export[1][0]->sumexport - $import[1][0]->sumimport,$export[2][0]->sumexport - $import[2][0]->sumimport,$export[3][0]->sumexport - $import[3][0]->sumimport,$export[4][0]->sumexport - $import[4][0]->sumimport,$export[5][0]->sumexport - $import[5][0]->sumimport,$export[6][0]->sumexport - $import[6][0]->sumimport,$export[7][0]->sumexport - $import[7][0]->sumimport,$export[8][0]->sumexport - $import[8][0]->sumimport,$export[9][0]->sumexport - $import[9][0]->sumimport,$export[10][0]->sumexport - $import[10][0]->sumimport,$export[11][0]->sumexport - $import[11][0]->sumimport,$export[12][0]->sumexport - $import[12][0]->sumimport])
                ->backgroundcolor("orange");
        return view('homefilter',compact(['countInven','countSup','countCus','countManager','chart']));
    }
}
