<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\RequestEditExport;
use App\Http\Requests\RequestEditImport;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Exports\ExportExcel;
use App\Exports\ImportExcel;
use App\Exports\ExportFilterExcel;
use App\Exports\ImportFilterExcel;
use App\Charts\InventoryChart;
use App\Charts\InterestChart;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\User;
use App\Category;
use App\Inventory;
use App\Product;
use App\Inventory_product;
use App\User_inventory;
use App\Import;
use App\Export;
use App\Customer;
use Auth;
use Excel;
use Charts;

class InventoryDetailController extends Controller
{
    public function index()
    {
        $userId = DB::table('role_user')
                ->join('users','users.id','=','role_user.user_id')
                ->whereIn('role_user.role_id',[1,2])
                ->select('users.*')
                ->get();
        $adminInven = DB::table('inventories')->paginate(6);
        $userCurrent = Auth::id();
        $inventories = DB::table('user_inventory')
                    ->join('inventories', 'inventories.id', '=', 'user_inventory.inventory_id')
                    ->where('user_inventory.user_id', '=', $userCurrent)
                    ->paginate(6);
        $countAdminInven = $adminInven->count();
        $countInven = $inventories->count();
        return view('invendetail.index',compact(['inventories','adminInven','userId','userCurrent','countAdminInven','countInven']));
    }

    //Chuyển hướng sang view import detail
    public function importDetail($invenid)
    {
        $countImport = Import::where('inventory_id',$invenid)->count();
        $inventory = Inventory::where('id',$invenid)->first();
        $import =  DB::table('import')
                ->join('products', 'products.id', '=', 'import.product_id')
                ->join('suppliers', 'suppliers.id', '=', 'import.supplier_id')
                ->crossJoin('users', 'users.id', '=', 'import.user_id')
                ->where('import.inventory_id', '=', $invenid)
                ->select('import.*','products.name','users.username as username','suppliers.name as supname')
                ->orderBy('updated_at')
                ->paginate(6);
        $quantityImport = DB::table('import')
                        ->where('inventory_id', '=', $invenid)
                        ->select(DB::raw('sum(quantity_import) as sumimport'))
                        ->first();
        return view('invendetail.importdetail',compact(['invenid','import','countImport','inventory','quantityImport']));
    }

    //Chuyển hướng sang view export detail
    public function exportDetail($invenid)
    {
        $countExport = Export::where('inventory_id',$invenid)->count();
        $inventory = Inventory::where('id',$invenid)->first();
        $export =  DB::table('export')
                ->join('products', 'products.id', '=', 'export.product_id')
                ->join('customers', 'customers.id', '=', 'export.customer_id')
                ->crossJoin('users', 'users.id', '=', 'export.user_id')
                ->where('export.inventory_id', '=', $invenid)
                ->select('export.*','products.name as proname','customers.name as cusname','users.username as username')
                ->orderBy('updated_at')
                ->paginate(6);
        $quantityExport = DB::table('export')
                        ->where('inventory_id', '=', $invenid)
                        ->select(DB::raw('sum(quantity_export) as sumexport'))
                        ->first();
        return view('invendetail.exportdetail',compact(['invenid','export','countExport','inventory','quantityExport']));
    }

    public function exportEdit($invenid, $proid, $exportid)
    {
        $export = Export::where('id',$exportid)->first();
        $customers = Customer::all();
        $product = Product::findOrfail($proid);
        return view('invendetail.editexport',compact(['export','customers','invenid','product']));
    }

    //Hàm sửa thông tin export
    public function postExportEdit($invenid, $proid, $exportid, RequestEditExport $request)
    {
        try {
            DB::beginTransaction();
            $quanProReturn = DB::table('import')
                            ->where([
                                ['inventory_id','=',$invenid],
                                ['product_id','=',$proid],
                                ['type','=',1],
                            ])
                            ->select(DB::raw('sum(quantity_import) as quanReturn'))
                            ->groupBy('product_id')
                            ->first();
            $totalQuanExport = DB::table('export')
                            ->where([
                                ['inventory_id','=',$invenid],
                                ['product_id','=',$proid],
                            ])
                            ->select(DB::raw('sum(quantity_export) as quanEx'))
                            ->groupBy('product_id')
                            ->first();
            $editExport = Export::where('id',$exportid)->first();
            $quanExportUpdate = ($totalQuanExport->quanEx - $editExport->quantity_export) + $request->quantity_export;
            if ($quanExportUpdate < $quanProReturn->quanReturn) {
                $request->session()->flash('error', 'Quantity export less than quantity product returned !');
                    return redirect()->back();
            }

            $inventoryProduct = DB::table('inventory_product')->where(['inventory_id' => $invenid,
                'product_id' => $proid])->first();
            if ($request->quantity_export > ($inventoryProduct->real_quantity + $editExport->quantity_export) && $inventoryProduct->real_quantity != 0) {
                $request->session()->flash('error', 'Quantity export larger than current quantity');
                    return redirect()->back();
            }
            //Cập nhập lại số lượng thực trước khi update
            $realQuantity = (($inventoryProduct->real_quantity) + ($editExport->quantity_export)) - ($request->quantity_export);
            $request->merge(['real_quantity'=>$realQuantity]);
            Inventory_product::where(['inventory_id' => $invenid,
                'product_id' => $proid])->update($request->only('real_quantity'));

            $editExport->customer_id = $request->customer_id;
            $editExport->price_export = $request->price_export;
            $editExport->quantity_export = $request->quantity_export;
            $editExport->status = ($inventoryProduct->newest_price_import > $request->price_export) ? '1' : '0';
            $editExport->updated_at = Carbon::now()->format('Y-m-d');
            $editExport->save();

            $totalProduct = DB::table('inventory_product')->where('inventory_id',$invenid)->sum('real_quantity');
            $inventoryUpdate = Inventory::find($invenid);
            $inventoryUpdate->total = $totalProduct;
            $inventoryUpdate->the_rest = $inventoryUpdate->max - $totalProduct;
            $inventoryUpdate->save();

            $countExport = Export::where('inventory_id',$invenid)->count();
            $quantityExport = DB::table('export')
                        ->where('inventory_id', '=', $invenid)
                        ->select(DB::raw('sum(quantity_export) as sumexport'))
                        ->first();
            $inventory = Inventory::where('id',$invenid)->first();
            $export =  DB::table('export')
                    ->join('products', 'products.id', '=', 'export.product_id')
                    ->join('customers', 'customers.id', '=', 'export.customer_id')
                    ->crossJoin('users', 'users.id', '=', 'export.user_id')
                    ->where('export.inventory_id', '=', $invenid)
                    ->select('export.*','products.name as proname','customers.name as cusname','users.username as username')
                    ->paginate(6);
            DB::commit();
            return view('invendetail.exportdetail',compact(['invenid','quantityExport','export','countExport','inventory']));
        } catch (Exception $e) {
            DB::rollBack();
        }
    }

    public function importEdit($invenid, $proid, $importid)
    {
        $import = Import::where('id',$importid)->first();
        $product = Product::findOrfail($proid);
        return view('invendetail.editimport',compact(['import','product','invenid','proid','importid']));
    }

    //Hàm sửa thông tin import detail
    public function postImportEdit($invenid, $proid, $importid, RequestEditImport $request)
    {
        try {
            DB::beginTransaction();
            $editImport = Import::where('id',$importid)->first();
            $inventoryProduct = DB::table('inventory_product')->where(['inventory_id' => $invenid,
                'product_id' => $proid])->first();
            
            //Cập nhập lại số lượng thực trước khi update
            $quantityEx = $editImport->quantity_import - $inventoryProduct->real_quantity;
            $realQuantity = (($inventoryProduct->real_quantity) - ($editImport->quantity_import)) + ($request->quantity_import);
            $inventory = Inventory::where('id',$invenid)->first();
            if($realQuantity > $inventory->max || ($realQuantity > $inventory->the_rest && $inventory->the_rest != 0)){
                $request->session()->flash('error', 'Inventory overflow , Maximum capacity of this repository is '.$inventory->max.', the rest capacity of this repository is '.$inventory->the_rest.' !!!');
                return redirect()->back();
            }
            if ($request->quantity_import < $quantityEx) {
                $request->session()->flash('error', 'The amount to be imported must not be less than the amount exported !');
                return redirect()->back();
            }
            $request->merge(['real_quantity'=>$realQuantity]);
            Inventory_product::where(['inventory_id' => $invenid,
                'product_id' => $proid])->update($request->only('real_quantity'));

            $editImport->price_import = $request->price_import;
            $editImport->quantity_import = $request->quantity_import;
            $editImport->updated_at = Carbon::now()->format('Y-m-d');
            $editImport->save();

            $totalProduct = DB::table('inventory_product')->where('inventory_id',$invenid)->sum('real_quantity');
            $inventoryUpdate = Inventory::find($invenid);
            $inventoryUpdate->total = $totalProduct;
            $inventoryUpdate->the_rest = $inventoryUpdate->max - $totalProduct;
            $inventoryUpdate->save();

            $countImport = Import::where('inventory_id',$invenid)->count();
            $quantityImport = DB::table('import')
                        ->where('inventory_id', '=', $invenid)
                        ->select(DB::raw('sum(quantity_import) as sumimport'))
                        ->first();
            $import =  DB::table('import')
                    ->join('products', 'products.id', '=', 'import.product_id')
                    ->join('suppliers', 'suppliers.id', '=', 'import.supplier_id')
                    ->crossJoin('users', 'users.id', '=', 'import.user_id')
                    ->where('import.inventory_id', '=', $invenid)
                    ->select('import.*','products.name','users.username as username','suppliers.name as supname')
                    ->paginate(6);
            DB::commit();
            return view('invendetail.importdetail',compact(['invenid','quantityImport','import','countImport','inventory']));
        } catch (Exception $e) {
            DB::rollBack();
        }
    }

    //Hàm lấy thông tin các sản phẩm bán chạy
    public function getTopSelling($invenid)
    {
        $export =  DB::table('products')
                ->join('export', 'export.product_id', '=', 'products.id')
                ->select('export.*','products.*',DB::raw('sum(export.quantity_export) as sumexport'))
                ->where('export.inventory_id', '=', $invenid)
                ->groupBy('export.product_id','export.inventory_id')
                ->orderBy('sumexport', 'desc')
                ->limit(3)
                ->get();

        $inventory = Inventory::where('id',$invenid)->first();
        return view('invendetail.topselling',compact(['invenid','export','inventory']));
    }

    //Hàm lọc thông tin Export Detail
    public function filterExport(Request $request)
    {
        $invenid = $request->invenid;
        $queryExport = Export::query()
                    ->key($request)
                    ->from($request)
                    ->to($request)
                    ->fromTo($request);
        
        $export = $queryExport->get();
        $inventory = Inventory::where('id',$invenid)->first();
        $countExport = $queryExport->count();
        return view('invendetail.filterexport',compact(['invenid','export','inventory','countExport']));
    }

    //Hàm lọc thông tin Import Detail
    public function filterImport(Request $request)
    {
        $invenid = $request->invenid;
        $queryImport = Import::query()
                    ->key($request)
                    ->from($request)
                    ->to($request)
                    ->fromTo($request);
        
        $import = $queryImport->get();
        $countImport = $queryImport->count();
        $inventory = Inventory::where('id',$invenid)->first();
        return view('invendetail.filterimport',compact(['invenid','import','countImport','inventory']));
    }

    //Hàm xuất báo cáo xuất kho ra file excel
    public function exportExcel($invenid)
    {
        return Excel::download(new ExportExcel($invenid), 'ExportReport.xlsx');
    }

    //Hàm xuất báo cáo nhập kho ra file excel
    public function importExcel($invenid)
    {
        return Excel::download(new ImportExcel($invenid), 'ImportReport.xlsx');
    }

    //Hàm xuất báo cáo xuất kho ra file excel (đã được filter)
    public function exportFilterExcel($invenid,$key,$from,$to)
    {
        return Excel::download(new ExportFilterExcel($invenid,$key,$from,$to),'ExportReport.xlsx');
    }

    //Hàm xuất báo cáo nhập kho ra file excel (đã được filter)
    public function importFilterExcel($invenid,$key,$from,$to)
    {
        return Excel::download(new ImportFilterExcel($invenid,$key,$from,$to),'ImportReport.xlsx');
    }

    //Hàm lấy ra các sp bị xả hàng
    public function dischargeGoods($invenid)
    {
        $inventory = Inventory::where('id',$invenid)->first();
        $listDischarge = DB::table('products')
                ->join('export', 'export.product_id', '=', 'products.id')
                ->join('inventory_product', 'inventory_product.product_id', '=', 'products.id')
                ->select('export.*','products.*', 'inventory_product.newest_price_import',DB::raw('sum(export.quantity_export) as sumexport'))
                ->where([['export.inventory_id', '=', $invenid],['export.status','=',1]])
                ->groupBy('export.product_id','export.inventory_id')
                ->paginate(8);
        return view('invendetail.listprodischarge',compact(['listDischarge','inventory','invenid']));
    }

    //Hàm hiển thị biểu đồ
    public function chart($invenid)
    {
        $inventory = Inventory::where('id',$invenid)->first();
        for ($i=1; $i <= 12; $i++) { 
            $export[$i] = DB::table('export')
            ->where([['inventory_id','=',$invenid],[DB::raw("DATE_FORMAT(created_at,'%Y')"),date('Y')],[DB::raw("DATE_FORMAT(created_at,'%m')"),$i]])
            ->select(DB::raw('SUM(quantity_export) AS sumexport'))
            ->get();
        }
        for ($i=1; $i <= 12; $i++) { 
            $priceExport[$i] = DB::table('export')
            ->where([['inventory_id','=',$invenid],[DB::raw("DATE_FORMAT(created_at,'%Y')"),date('Y')],[DB::raw("DATE_FORMAT(created_at,'%m')"),$i]])
            ->select(DB::raw('SUM(price_export*quantity_export) AS sumpriceexport'))
            ->get();
        }
        for ($i=1; $i <= 12; $i++) { 
            $import[$i] = DB::table('import')
            ->where([['inventory_id','=',$invenid],[DB::raw("DATE_FORMAT(created_at,'%Y')"),date('Y')],[DB::raw("DATE_FORMAT(created_at,'%m')"),$i]])
            ->select(DB::raw('SUM(quantity_import) AS sumimport'))
            ->get();
        }
        for ($i=1; $i <= 12; $i++) { 
            $priceImport[$i] = DB::table('import')
            ->where([['inventory_id','=',$invenid],[DB::raw("DATE_FORMAT(created_at,'%Y')"),date('Y')],[DB::raw("DATE_FORMAT(created_at,'%m')"),$i]])
            ->select(DB::raw('SUM(price_import*quantity_import) AS sumpriceimport'))
            ->get();
        }

        $chart = new InventoryChart;
        $chart->labels(['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC']);
        $chart->title("Import and Export Chart");
        $chart->dataset('Quantity export', 'bar', [$export[1][0]->sumexport,$export[2][0]->sumexport,$export[3][0]->sumexport,$export[4][0]->sumexport,$export[5][0]->sumexport,$export[6][0]->sumexport,$export[7][0]->sumexport,$export[8][0]->sumexport,$export[9][0]->sumexport,$export[10][0]->sumexport,$export[11][0]->sumexport,$export[12][0]->sumexport])
                ->backgroundcolor("#c52828");
        $chart->dataset('Quantity import', 'bar', [$import[1][0]->sumimport,$import[2][0]->sumimport,$import[3][0]->sumimport,$import[4][0]->sumimport,$import[5][0]->sumimport,$import[6][0]->sumimport,$import[7][0]->sumimport,$import[8][0]->sumimport,$import[9][0]->sumimport,$import[10][0]->sumimport,$import[11][0]->sumimport,$import[12][0]->sumimport])
                ->backgroundcolor("#307fad");

        $interestChart = new InterestChart;
        $interestChart->labels(['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC']);
        $interestChart->title("Interest Chart");
        $interestChart->dataset('Price export', 'line', [$priceExport[1][0]->sumpriceexport,$priceExport[2][0]->sumpriceexport,$priceExport[3][0]->sumpriceexport,$priceExport[4][0]->sumpriceexport,$priceExport[5][0]->sumpriceexport,$priceExport[6][0]->sumpriceexport,$priceExport[7][0]->sumpriceexport,$priceExport[8][0]->sumpriceexport,$priceExport[9][0]->sumpriceexport,$priceExport[10][0]->sumpriceexport,$priceExport[11][0]->sumpriceexport,$priceExport[12][0]->sumpriceexport])
                ->backgroundcolor("yellow");
        $interestChart->dataset('Price import', 'line', [$priceImport[1][0]->sumpriceimport,$priceImport[2][0]->sumpriceimport,$priceImport[3][0]->sumpriceimport,$priceImport[4][0]->sumpriceimport,$priceImport[5][0]->sumpriceimport,$priceImport[6][0]->sumpriceimport,$priceImport[7][0]->sumpriceimport,$priceImport[8][0]->sumpriceimport,$priceImport[9][0]->sumpriceimport,$priceImport[10][0]->sumpriceimport,$priceImport[11][0]->sumpriceimport,$priceImport[12][0]->sumpriceimport])
                ->backgroundcolor("#3cbc3d");
        return view('invendetail.chart',compact(['interestChart','chart','invenid','inventory']));
    }

    //Hàm filter chart
    public function getFilter($invenid,Request $request)
    {
        $inventory = Inventory::where('id',$invenid)->first();
        for ($i=1; $i <= 12; $i++) { 
            $export[$i] = DB::table('export')
            ->where([['inventory_id','=',$invenid],[DB::raw("DATE_FORMAT(created_at,$request->key)"),date('Y')],[DB::raw("DATE_FORMAT(created_at,'%m')"),$i]])
            ->select(DB::raw('SUM(quantity_export) AS sumexport'))
            ->get();
        }
        for ($i=1; $i <= 12; $i++) { 
            $priceExport[$i] = DB::table('export')
            ->where([['inventory_id','=',$invenid],[DB::raw("DATE_FORMAT(created_at,$request->key)"),date('Y')],[DB::raw("DATE_FORMAT(created_at,'%m')"),$i]])
            ->select(DB::raw('SUM(price_export*quantity_export) AS sumpriceexport'))
            ->get();
        }
        for ($i=1; $i <= 12; $i++) { 
            $import[$i] = DB::table('import')
            ->where([['inventory_id','=',$invenid],[DB::raw("DATE_FORMAT(created_at,$request->key)"),date('Y')],[DB::raw("DATE_FORMAT(created_at,'%m')"),$i]])
            ->select(DB::raw('SUM(quantity_import) AS sumimport'))
            ->get();
        }
        for ($i=1; $i <= 12; $i++) { 
            $priceImport[$i] = DB::table('import')
            ->where([['inventory_id','=',$invenid],[DB::raw("DATE_FORMAT(created_at,$request->key)"),date('Y')],[DB::raw("DATE_FORMAT(created_at,'%m')"),$i]])
            ->select(DB::raw('SUM(price_import*quantity_import) AS sumpriceimport'))
            ->get();
        }

        $chart = new InventoryChart;
        $chart->labels(['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC']);
        $chart->title("Import and Export Chart");
        $chart->dataset('Quantity export', 'bar', [$export[1][0]->sumexport,$export[2][0]->sumexport,$export[3][0]->sumexport,$export[4][0]->sumexport,$export[5][0]->sumexport,$export[6][0]->sumexport,$export[7][0]->sumexport,$export[8][0]->sumexport,$export[9][0]->sumexport,$export[10][0]->sumexport,$export[11][0]->sumexport,$export[12][0]->sumexport])
                ->backgroundcolor("#c52828");
        $chart->dataset('Quantity import', 'bar', [$import[1][0]->sumimport,$import[2][0]->sumimport,$import[3][0]->sumimport,$import[4][0]->sumimport,$import[5][0]->sumimport,$import[6][0]->sumimport,$import[7][0]->sumimport,$import[8][0]->sumimport,$import[9][0]->sumimport,$import[10][0]->sumimport,$import[11][0]->sumimport,$import[12][0]->sumimport])
                ->backgroundcolor("#307fad");

        $interestChart = new InterestChart;
        $interestChart->labels(['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC']);
        $interestChart->title("Interest Chart");
        $interestChart->dataset('Price export', 'line', [$priceExport[1][0]->sumpriceexport,$priceExport[2][0]->sumpriceexport,$priceExport[3][0]->sumpriceexport,$priceExport[4][0]->sumpriceexport,$priceExport[5][0]->sumpriceexport,$priceExport[6][0]->sumpriceexport,$priceExport[7][0]->sumpriceexport,$priceExport[8][0]->sumpriceexport,$priceExport[9][0]->sumpriceexport,$priceExport[10][0]->sumpriceexport,$priceExport[11][0]->sumpriceexport,$priceExport[12][0]->sumpriceexport])
                ->backgroundcolor("yellow");
        $interestChart->dataset('Price import', 'line', [$priceImport[1][0]->sumpriceimport,$priceImport[2][0]->sumpriceimport,$priceImport[3][0]->sumpriceimport,$priceImport[4][0]->sumpriceimport,$priceImport[5][0]->sumpriceimport,$priceImport[6][0]->sumpriceimport,$priceImport[7][0]->sumpriceimport,$priceImport[8][0]->sumpriceimport,$priceImport[9][0]->sumpriceimport,$priceImport[10][0]->sumpriceimport,$priceImport[11][0]->sumpriceimport,$priceImport[12][0]->sumpriceimport])
                ->backgroundcolor("#3cbc3d");
        return view('invendetail.chartfilter',compact(['interestChart','chart','invenid','inventory']));
    }

}
