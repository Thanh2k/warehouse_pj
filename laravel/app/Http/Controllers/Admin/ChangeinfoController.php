<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\RequestChangeinfo;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Model;
use App\User;
use Auth;

class ChangeinfoController extends Controller
{
    //Hàm chuyển sang view thay đổi thông tin
    public function change()
    {
        return view('changeinfo');
    }

    //Hàm update thông tin tài khoản sau lần đầu đăng nhập
    public function postChange(RequestChangeinfo $request)
    {
        $user = User::find(Auth::id());
        if(Hash::check($request->password, $user->password)){
            $request->session()->flash('error', 'The new password must be different from the current password');
            return redirect()->back();
        }
        //Hash password
        $password = bcrypt($request->password);
        $request->merge(['password' => $password]);
        $first_login_status = 1;
        $request->merge(['first_login_status' => $first_login_status]);
        $request->offsetUnset('_token');
        $request->offsetUnset('confirm_password');
        User::where(['id' => Auth::id()])->update($request->all());
        return redirect()->route('home');
    }
}
