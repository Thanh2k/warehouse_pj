<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\RequestImportDataByExcelFile;
use App\Http\Requests\RequestImportPlusProduct;
use App\Http\Requests\RequestImportProduct;
use App\Http\Requests\RequestExportProduct;
use App\Http\Requests\RequestEditProduct;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\Controller;
use App\Exports\TemplateImportData;
use Illuminate\Support\Facades\DB;
use App\Exports\ExcelProduct;
use App\Imports\ImportImport;
use Illuminate\Http\Request;
use App\Inventory_product;
use Carbon\Carbon;
use App\Category;
use App\Inventory;
use App\Product;
use App\Import;
use App\Export;
use App\Supplier;
use App\Customer;
use App\User;
use App\Unit;
use File;
use Auth;
use Excel;

class ProductController extends Controller
{
    //Chuyển hướng sang view product
    public function index($invenid)
    {
        $invenProduct =  DB::table('products')
                ->join('inventory_product', 'inventory_product.product_id', '=', 'products.id')
                ->join('categories', 'categories.id', '=', 'products.category_id')
                ->join('units', 'units.id', '=', 'products.unit_id')
                ->join('import', 'import.product_id', '=', 'products.id')
                ->crossJoin('suppliers', 'suppliers.id', '=', 'import.supplier_id')
                ->where('inventory_product.inventory_id', '=', $invenid)
                ->select('inventory_product.*', 'products.*','categories.name as catname','units.name as unitname','suppliers.name as supname')
                ->groupBy('products.id','inventory_product.id')
                ->orderBy('products.id','DESC')
                ->paginate(5);
        $inventory = Inventory::where('id',$invenid)->first();
        $count = DB::table('products')
                ->join('import', 'import.product_id', '=', 'products.id')
                ->where('import.inventory_id', '=', $invenid)
                ->count();
        return view('products.index',compact(['invenProduct','inventory','invenid','count']));
    }

    //Chuyển hướng sang view import
    public function import($invenid)
    {
        $categories = Category::all();
        $units = Unit::all();
        $supplier = Supplier::all();
        return view('products.import',compact(['categories', 'invenid','supplier','units']));
    }

    //Lưu thông tin import
    public function postImport(RequestImportProduct $request,$invenid)
    {
        try {
            DB::beginTransaction();
            $inventory_product = DB::table('inventory_product')->where(['inventory_id'=>$invenid])->pluck('product_id');
            $products = Product::all();
            foreach ($products as $p) {
                if ($inventory_product->contains($p->id)) {
                    if($p->name == $request->name){
                        $request->session()->flash('error', 'Product name already exists !!!');
                        return redirect()->back();
                    }
                }
            }
            $inventory = Inventory::find($invenid);
            if($request->quantity_import > $inventory->max || ($request->quantity_import > $inventory->the_rest && $inventory->the_rest != 0)){
                $request->session()->flash('error', 'Inventory overflow , Maximum capacity of this repository is '.$inventory->max.', the rest capacity of this repository is '.$inventory->the_rest.' !!!');
                return redirect()->back();
            }

            $product = new Product; 
            if($request->hasFile('image')){
                $fileName = time().'-'.$request->file('image')->getClientOriginalName();
                $imgPath = public_path('/img/product');
                $request->file('image')->move($imgPath, $fileName);
                $product->image = $fileName;
            }
            $product->name = $request->name;
            $product->category_id = $request->category_id;
            $product->unit_id = $request->unit_id;
            $product->save();

            $import = new Import;
            $import->inventory_id = $invenid;
            $import->user_id = Auth::id();
            $import->product_id = $product->id;
            $import->supplier_id = $request->supplier_id;
            $import->quantity_import = $request->quantity_import;
            $import->price_import = $request->price_import;
            $import->created_at = Carbon::now()->format('Y-m-d');
            $import->updated_at = Carbon::now()->format('Y-m-d');
            $import->save();

            $inventoryProduct = new Inventory_product; 
            $inventoryProduct->inventory_id = $invenid; 
            $inventoryProduct->product_id = $product->id; 
            $inventoryProduct->real_quantity = $request->quantity_import; 
            $inventoryProduct->newest_price_import = $request->price_import; 
            $inventoryProduct->save(); 

            $totalProduct = DB::table('inventory_product')->where('inventory_id',$invenid)->sum('real_quantity');
            $inventory->total = $totalProduct;
            $inventory->the_rest = $inventory->the_rest - $request->quantity_import;
            $inventory->save();
            DB::commit();
            return redirect()->route('product',['invenid'=>$invenid]);
        } catch (Exception $e) {
            DB::rollBack();
        }
    }

    //Import thêm sản phẩm cho kho
    public function importPlus($invenid,$idpro)
    {
        $product = Product::findOrfail($idpro);
        $import = Import::where([['inventory_id','=',$invenid],['product_id','=',$idpro]])->first();
        $suppliers = Supplier::all();
        return view('products.importplus',compact(['product', 'invenid','idpro','import','suppliers']));
    }

    //Import thêm sản phẩm cho kho
    public function postImportPlus(RequestImportPlusProduct $request,$invenid,$proid)
    {
        try {
            DB::beginTransaction();
            $inventory = Inventory::find($invenid);
            if($request->quantity_import > $inventory->the_rest){
                $request->session()->flash('error', 'Inventory overflow, The rest capacity of this repository is '.$inventory->the_rest.' !!!');
                return redirect()->back();
            }

            $import = new Import;
            $import->inventory_id = $invenid;
            $import->user_id = Auth::id();
            $import->product_id = $proid;
            $import->supplier_id = $request->supplier_id;
            $import->quantity_import = $request->quantity_import;
            $import->price_import = $request->price_import;
            $import->created_at = Carbon::now()->format('Y-m-d');
            $import->updated_at = Carbon::now()->format('Y-m-d');
            $import->save();

            $inventoryProduct = DB::table('inventory_product')->where(['inventory_id' => $invenid,
                'product_id' => $proid])->first();
            $realQuantity = ($inventoryProduct->real_quantity) + ($request->quantity_import);
            $request->merge(['real_quantity'=>$realQuantity]);
            $request->merge(['newest_price_import'=>$request->price_import]);
            $inventoryProduct = Inventory_product::where(['inventory_id' => $invenid,
                'product_id' => $proid])->update($request->only('real_quantity','newest_price_import'));

            $totalProduct = DB::table('inventory_product')->where('inventory_id',$invenid)->sum('real_quantity');
            $inventory->total = $totalProduct;
            $inventory->the_rest = $inventory->the_rest - $request->quantity_import;
            $inventory->save();
            DB::commit();
            return redirect()->route('product',['invenid'=>$invenid]);
        } catch (Exception $e) {
            DB::rollBack();
        }
    }

    //Chuyển hướng sang view export
    public function export($invenid,$proid)
    {
        $invenProduct =  DB::table('products')
                ->join('inventory_product', 'inventory_product.product_id', '=', 'products.id')
                ->where([['inventory_product.inventory_id', '=', $invenid],['inventory_product.product_id', '=', $proid]])
                ->select('inventory_product.*', 'products.*')
                ->groupBy('products.id','inventory_product.id')
                ->first();
        $product = Product::findOrfail($proid);
        $customers = Customer::all();
        return view('products.export',compact(['invenid','invenProduct','customers','proid','product']));
    }

    //Lưu thông tin export
    public function postExport(RequestExportProduct $request,$invenid,$proid)
    {
        try {
            DB::beginTransaction();
            $inventoryProduct = DB::table('inventory_product')->where(['inventory_id' => $invenid,
            'product_id' => $proid])->first();
            if ($inventoryProduct->real_quantity == 0) {
                $request->session()->flash('error', 'Out of stock');
                    return redirect()->back();
            }
            if ($request->quantity_export > $inventoryProduct->real_quantity) {
                $request->session()->flash('error', 'Quantity export larger than current quantity');
                    return redirect()->back();
            }
            
            $export = new Export;
            $export->inventory_id = $invenid;
            $export->user_id = Auth::id();
            $export->product_id = $proid;
            $export->customer_id = $request->customer_id;
            $export->quantity_export = $request->quantity_export;
            $export->price_export = $request->price_export;
            $export->status = ($inventoryProduct->newest_price_import > $request->price_export) ? '1' : '0';
            $export->created_at = Carbon::now()->format('Y-m-d');
            $export->updated_at = Carbon::now()->format('Y-m-d');
            $export->save();

            $realQuantity = ($inventoryProduct->real_quantity) - ($request->quantity_export);
            $request->merge(['real_quantity'=>$realQuantity]);
            $inventoryProduct = Inventory_product::where(['inventory_id' => $invenid,
                'product_id' => $proid])->update($request->only('real_quantity'));

            $totalProduct = DB::table('inventory_product')->where('inventory_id',$invenid)->sum('real_quantity');
            $inventory = Inventory::find($invenid);
            $inventory->total = $totalProduct;
            $inventory->the_rest = $inventory->the_rest + $request->quantity_export;
            $inventory->save();
            DB::commit();
            return redirect()->route('product',['invenid'=>$invenid]);
        } catch (Exception $e) {
            DB::rollBack();
        }
    }

    //Hàm chuyển hướng sang view product
    public function viewProduct($invenid,$idpro)
    {
        $invenProduct =  DB::table('products')
                ->join('inventory_product', 'inventory_product.product_id', '=', 'products.id')
                ->join('categories', 'categories.id', '=', 'products.category_id')
                ->join('units', 'units.id', '=', 'products.unit_id')
                ->join('import', 'import.product_id', '=', 'products.id')
                ->crossJoin('suppliers', 'suppliers.id', '=', 'import.supplier_id')
                ->select('inventory_product.*', 'products.*','categories.name as catname','units.name as unitname','suppliers.name as supname',DB::raw('sum(import.quantity_import) as sumimport'))
                ->where([
                    ['inventory_product.inventory_id', '=', $invenid],
                    ['inventory_product.product_id', '=', $idpro],
                ])
                ->groupBy(['inventory_product.product_id','inventory_product.inventory_id'])
                ->first();
        return view('products.view',compact(['invenProduct','idpro','invenid']));
    }

    //Hàm chuyển hướng sang view edit product
    public function edit($id, $invenid)
    {
        $product =  DB::table('products')
                ->join('import', 'import.product_id', '=', 'products.id')
                ->join('units', 'units.id', '=', 'products.unit_id')
                ->crossJoin('suppliers', 'suppliers.id', '=', 'import.supplier_id')
                ->where([['import.inventory_id', '=', $invenid],['import.product_id','=',$id]])
                ->select('suppliers.id as supid','products.*','units.id as unitid')
                ->first();
        $categories = Category::all();
        $units = Unit::all();
        $suppliers = Supplier::all();
        return view('products.edit',compact(['product','categories','invenid','suppliers','units']));
    }

    //Hàm edit product
    public function postEdit($id, RequestEditProduct $request, $invenid)
    {
        try {
            DB::beginTransaction();
            $invenPro = DB::table('inventory_product')->where('inventory_id', '=', $invenid)->pluck('product_id');
            $products = DB::table('products')->where('id', '<>', $id)->get();
            foreach ($products as $p) {
                if ($invenPro->contains($p->id)) {

                    if($request->name == $p->name){
                        $request->session()->flash('error', 'Product name already exists');
                        return redirect()->back();
                    }
                }
            }

            $product = Product::find($id); 
            $product->name = $request->name;
            $product->category_id = $request->category_id;
            $product->unit_id = $request->unit_id;
            $product->updated_at = Carbon::now('Asia/Ho_Chi_Minh');
            
            if ($request->hasFile('image')) { 
                $file = $request->file('image');
                $fileName = time().'.'.$file->getClientOriginalName();
                $imagePath = public_path('/img/product');
                $imagePathdelete = 'img/product/'.$product->image;
                
                if(file_exists($imagePathdelete) && !empty($product->image)){
                    unlink($imagePathdelete);
                    $file->move($imagePath, $fileName);
                    $product->image = $fileName;
                }else{
                    $file->move($imagePath, $fileName);
                    $product->image = $fileName;
                }
            }
            $product->save();

            $import = Import::where([['inventory_id', '=', $invenid],['product_id','=',$id]])->first();
            $import->supplier_id = $request->supplier_id;
            $import->save();
            DB::commit();
            return redirect()->route('product',['invenid'=>$invenid]);
        } catch (Exception $e) {
            DB::rollBack();
        }
    }

    //Hàm xóa product
    public function delete($id, $invenid)
    {
        $nameImage = Product::find($id);
        $image_path = "img/product/".$nameImage->image;  
        if(file_exists($image_path) && !empty($nameImage->image)) {
            unlink($image_path);
        }
        Product::find($id)->delete();
        $totalProduct = DB::table('inventory_product')->where('inventory_id',$invenid)->sum('real_quantity');
        $inventory = Inventory::find($invenid);
        $inventory->total = $totalProduct;
        $inventory->the_rest = $inventory->max - $totalProduct;
        $inventory->save();
        return redirect()->back();
    }

    //Hàm export ra file excel
    public function excelProduct($invenid)
    {
        return Excel::download(new ExcelProduct($invenid), 'ProductList.xlsx');
    }

    //Hàm download template import data
    public function templateImport()
    {
        return Excel::download(new TemplateImportData(), 'TemplateImportData.xlsx');
    }

    //Hàm filter product
    public function getFilter(Request $request)
    {
        $invenid = $request->invenid;
        $queryProduct = Product::query()
                    ->key($request);
        $invenProduct = $queryProduct->get();
        $countResult = $invenProduct->count();
        $inventory = Inventory::where('id',$invenid)->first();
        return view('products.filter',compact(['invenProduct','inventory','invenid','countResult']));
    }

    //Ham lay thong tin ds hang ton kho
    public function inventoryList($invenid)
    {
        $dateNow = Carbon::now();
        $proIdExport =  DB::table('products')
                ->join('export', 'export.product_id', '=', 'products.id')
                ->where([['export.inventory_id', '=', $invenid]])
                ->pluck('product_id')->toArray();
        $invenList =  DB::table('products')
                ->join('import', 'import.product_id', '=', 'products.id')
                ->join('inventory_product', 'inventory_product.product_id', '=', 'products.id')
                ->select('import.*','products.created_at as dateimport','products.name as proname','products.id as proid','inventory_product.real_quantity')
                ->where([['import.inventory_id', '=', $invenid]])
                ->whereNotIn('products.id', $proIdExport)
                ->groupBy('import.product_id')
                ->get();
        return view('products.invenlist',compact(['invenList','dateNow','invenid']));
    }

    //Ham chuyen huong sang view import product by excel file
    public function importByExcelFile($invenid)
    {
        return view('imports.importexcel',compact(['invenid']));
    }

    //Hàm nhập product bằng file excel
    public function postImportByExcelFile(RequestImportDataByExcelFile $request,$invenid)
    {
        Excel::import(new ImportImport($invenid), $request->file('import_file'));
        return view('imports.importexcel',compact(['invenid']));
    }
}
