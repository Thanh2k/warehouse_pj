<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\RequestCreateUnit;
use App\Http\Requests\RequestEditUnit;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Unit;
use Auth;

class UnitController extends Controller
{
    public function index()
    {
        $units = Unit::paginate(8);
        $count = $units->count();
        return view('unit.index',compact(['units','count']));
    }

    //Hàm chuyển hướng sang view add unit
    public function add()
    {
        return view('unit.add');
    }

    //Hàm create unit
    public function postAdd(RequestCreateUnit $request)
    {
        $unit = new Unit;
        $unit->name = $request->name;
        $unit->save();

        return redirect()->route('unit');
    }

    //Hàm chuyển hướng sang view edit unit
    public function edit($id)
    {
        $unit = Unit::findOrfail($id);
        return view('unit.edit',compact(['unit']));
    }

    //Hàm edit unit
    public function postEdit($id, RequestEditUnit $request)
    {
        $unit = DB::table('units')->where('id', '<>', $id)->get();
        foreach ($unit as $c) {
            if($request->name == $c->name){
                $request->session()->flash('error', 'Unit already exists');
                return redirect()->back();
            }
        }
        Unit::where(['id' => $id])->update($request->only('name'));
        return redirect()->route('unit');
    }

    //Hàm xóa unit
    public function delete($id,Request $request)
    {
        $this->authorize('Super admin');
        $unitPro = DB::table('products')
                ->join('units', 'units.id', '=', 'products.unit_id')
                ->select('products.*')
                ->get()->toArray();
        foreach ($unitPro as $uP) {
            if ($uP->unit_id == $id) {
                $request->session()->flash('error', 'Unit already has product(s) !!!');
                return redirect()->back();
            }
        }
        Unit::find($id)->delete();
        return redirect()->back();
    }
}
