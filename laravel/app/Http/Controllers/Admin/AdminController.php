<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\RequestForgotPassword;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\RequestLogin;
use Illuminate\Http\Request;
use App\User;
use Auth;
use Mail;

class AdminController extends Controller
{
    public function index()
    {
        return view('user.index');
    }

    public function login()
    {
        return view('login');
    }

    //Hàm check điều kiện login
    public function postLogin(RequestLogin $request)
    {
        if (Auth::attempt($request->only('username','password'),$request->has('remember'))) {
            
            if (Auth::user()->first_login_status == 1) {
                return redirect()->route('home');
            }else{
                return redirect()->route('changeinfo');
            }
        }else{
            return redirect()->back()->with('error','Username or Password incorrect !');
        }
    }

    //Hàm forgot password
    public function forgotPass()
    {
        return view('forgotpass');
    }

    //Hàm post forgot password
    public function postForgotPass(RequestForgotPassword $request)
    {
        $user = User::whereEmail($request->email)->first();
        if ($user == NULL) {
            return redirect()->back()->with('error','Email not exist !!!');
        }
        $newPass = mt_rand();
        $password = bcrypt($newPass);
        $request->merge(['password'=>$password]);
        User::where(['id' => $user->id])->update($request->only('password'));
        $this->sendMail($user, $newPass);
        return redirect()->back()->with('success','The new password sent to your email !!!');
    }

    public function sendMail($user, $newPass)
    {
        Mail::send(
            'emails.forgotpass',
            ['user' => $user,'newPass' => $newPass],
            function($message) use ($user)
            {
                $message->to($user->email);
                $message->subject("$user->username, reset your password.");
            }
        );
    }

    //Hàm logout
    public function logout()
    {
        Auth::logout();
        return redirect()->route('login');
    }
}
