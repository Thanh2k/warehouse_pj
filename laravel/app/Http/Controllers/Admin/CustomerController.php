<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\RequestCreateCustomer;
use App\Http\Requests\RequestEditCustomer;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Customer;
use App\User;
use App\Role;
use Auth;

class CustomerController extends Controller
{
    public function index()
    {
        $customers = Customer::paginate(8);
        $count = $customers->count();
        return view('customer.index',compact(['customers','count']));
    }

    //Hàm chuyển hướng sang view add customer
    public function add()
    {
        return view('customer.add');
    }

    //Hàm create customer
    public function postAdd(RequestCreateCustomer $request)
    {
        $customer = new Customer;
        $customer->name = $request->name;
        $customer->email = $request->email;
        $customer->address = $request->address;
        $customer->phone_number = $request->phone_number;
        $customer->save();
        return redirect()->route('customer');
    }

    //Hàm chuyển hướng sang view edit customer
    public function edit($id)
    {
        $customer = Customer::findOrfail($id);
        return view('customer.edit',compact(['customer']));
    }

    //Hàm edit customer
    public function postEdit($id, RequestEditCustomer $request)
    {
        $customer = DB::table('customers')->where('id', '<>', $id)->get();
        foreach ($customer as $c) {
            if($request->email == $c->email){
                $request->session()->flash('error', 'Email address already exists');
                return redirect()->back();
            }
            if($request->phone_number == $c->phone_number && $request->phone_number != null){
                $request->session()->flash('error', 'Phone number already exists');
                return redirect()->back();
            }
        }
        Customer::where(['id' => $id])->update($request->only('name','address','email','phone_number'));
        return redirect()->route('customer');
    }

    //Hàm xóa customers
    public function delete($id,Request $request)
    {
        $this->authorize('Super admin');
        $cusExport = DB::table('customers')
                    ->join('export', 'export.customer_id', '=', 'customers.id')
                    ->select('export.customer_id')
                    ->groupBy('export.customer_id')
                    ->get();
        foreach ($cusExport as $cE) {
            if ($cE->customer_id == $id) {
                $request->session()->flash('error', 'Customer already has a transaction !!!');
                return redirect()->back();
            }
        }
        Customer::find($id)->delete();
        return redirect()->back();
    }

    //Hàm filter customer
    public function getFilter(Request $request)
    {
        $queryCustomer = Customer::query()
                    ->key($request);
        $customers = $queryCustomer->paginate(6);
        $countCus = $customers->count();
        return view('customer.filter',compact(['customers','countCus']));
    }
}
