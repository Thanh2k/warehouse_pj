<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\RequestCreateUser;
use App\Http\Requests\RequestEditUser;
use App\Http\Requests\RequestEditUserProfile;
use App\Http\Requests\RequestEditUserPassword;
use App\Http\Requests\RequestEditMultiUserPassword;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Mail\ChangePass;
use App\User;
use App\Role;
use App\Role_user;
use Auth;
use Mail;

class UserController extends Controller
{
    public function index()
    {
        $users = DB::table('role_user')
                ->join('users','users.id','=','role_user.user_id')
                ->join('roles','roles.id','=','role_user.role_id')
                ->where('users.id','<>',1)
                ->select('users.*','roles.name')
                ->paginate(8);
        $count = $users->count();
        return view('user.index',compact(['count','users']));
    }

    //Hàm chuyển hướng sang view add manager account
    public function add()
    {
        $this->authorize('Super admin');
        $roles = Role::where('id','<>',1)->get();
        return view('user.add',compact('roles'));
    }

    //Hàm tạo manager account
    public function postAdd(RequestCreateUser $request)
    {
        try {
            DB::beginTransaction();
            //Hash password
            $password = bcrypt($request->password);
            //Merge lại
            $request->merge(['password'=>$password]);
            $user_create = User::create($request->only('username','password'));
            \DB::table('role_user')->insert([
                'user_id' => $user_create->id,
                'role_id' => $request->roles,
            ]);
            DB::commit();
            return redirect()->route('user');
        } catch (Exception $e) {
            DB::rollBack();
        }
    }

    //Hàm chuyển hướng sang view edit user
    public function edit($id)
    {
        $model = User::find($id);
        $role = DB::table('role_user')->where('user_id',$id)->pluck('role_id');
        $roles = Role::where('id','<>',1)->get();

        return view('user.edit',[
            'model' => $model,
            'role' => $role,
            'roles' => $roles,
        ]);
    }

    //Hàm edit user
    public function postEdit($id, RequestEditUser $request)
    {
        try {
            DB::beginTransaction();
            $user = User::where('id', '<>', $id)->get();
            foreach ($user as $u) {
                if($request->username == $u->username){
                    $request->session()->flash('error', 'Manager name already exists');
                    return redirect()->back();
                }
            }
            //Hash password
            $password = bcrypt($request->password);
            $request->merge(['password'=>$password]);
            User::where(['id' => $id])->update($request->only('username','password'));
            DB::table('role_user')->where('user_id',$id)->delete();
            \DB::table('role_user')->insert([
                'user_id' => $id,
                'role_id' => $request->roles,
            ]);

            DB::commit();
            return redirect()->route('user');
        } catch (Exception $e) {
            DB::rollBack();
        }
    }

    //Hàm chuyển hướng sang view user
    public function view()
    {
        $id = Auth::id();
        $model = User::find($id);
        return view('user.view',[
            'model' => $model,
        ]);
    }

    //Hàm chuyển hướng sang view edit profile user
    public function editProfile($id)
    {
        $user = User::find($id);
        return view('user.editprofile',compact('user'));
    }

    //Hàm edit profile user
    public function postEditProfile($id, RequestEditUserProfile $request)
    {
        $request->offsetUnset('_token');
        $user = DB::table('users')->where('id', '<>', $id)->get();
        foreach ($user as $u) {
            if($request->email == $u->email){
                $request->session()->flash('error', 'Email address already exists');
                return redirect()->back();
            }
            if($request->phone_number == $u->phone_number && $request->phone_number != null){
                $request->session()->flash('error', 'Phone number already exists');
                return redirect()->back();
            }
        }
        User::where(['id' => $id])->update($request->all());
        return redirect()->route('user_view');
    }

    //Hàm chuyển hướng sang view edit password user
    public function editPassword($id)
    {
        $user = User::find($id);
        return view('user.editpassword',compact('user'));
    }

    //Hàm edit password user
    public function postEditPassword($id, RequestEditUserPassword $request)
    {
        $user = User::find($id);
        if(!Hash::check($request->old_password, $user->password)){
            $request->session()->flash('error', 'Password does not match with your old password');
            return redirect()->back();
        }
        if($request->old_password == $request->password){
            $request->session()->flash('error', 'The new password must be different from the current password');
            return redirect()->back();
        }
        
        //Hash password
        $password = bcrypt($request->password);
        //Merge lại
        $request->merge(['password'=>$password]);
        User::where(['id' => $id])->update($request->only('password'));
        $request->session()->flash('success', 'Password changed !!!');
        return redirect()->route('user_view');
    }

    public function editMultiPassword()
    {
        $this->authorize('Super admin');
        $users = User::where('id','<>',1)->get();
        return view('user.editmultipassword',compact(['users']));
    }

    public function postEditMultiPassword(RequestEditMultiUserPassword $request)
    {
        $this->authorize('Super admin');
        $id = $request->users;
        if (count($id) > 1) {
          for ($i = 0; $i < count($id); $i++) {
            $newPass = mt_rand(); 
            $password = bcrypt($newPass);
            $user = User::where('id',$id[$i])->first();
            User::where('id',$id[$i])->update(['password' => $password]);
            if ($user->email) {
                Mail::to($user->email)->send(new ChangePass($newPass,$user->username));
            }
          }
        }else{
            $newPass = mt_rand(); 
            $password = bcrypt($newPass);
            $user = User::where('id',$id)->first();
            User::where('id',$id)->update(['password' => $password]);
            if ($user->email) {
                Mail::to($user->email)->send(new ChangePass($newPass,$user->username));
            }
        }
        return redirect()->back()->with('success','Password has been changed !');
    }

    //Hàm xóa user
    public function delete($id,Request $request)
    {
        $this->authorize('Super admin');
        $userInven = DB::table('user_inventory')
                    ->join('users', 'users.id', '=', 'user_inventory.user_id')
                    ->select('user_inventory.user_id')
                    ->groupBy('user_inventory.user_id')
                    ->get();
        foreach ($userInven as $uI) {
            if ($uI->user_id == $id) {
                $request->session()->flash('error', 'This user is currently a inventory manager !!!');
                return redirect()->back();
            }
        }
        User::find($id)->delete();
        return redirect()->back();
    }
}
