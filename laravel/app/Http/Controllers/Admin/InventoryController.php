<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\RequestCreateInventory;
use App\Http\Requests\RequestEditInventory;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\User_inventory;
use App\Inventory;
use App\User;
use App\Role;
use Auth;

class InventoryController extends Controller
{
    public function index()
    {
        $inventories = DB::table('inventories')->paginate(8);
        $userInventory = DB::table('user_inventory')->get();
        $users = User::all();
        $count = $inventories->count();
        return view('inventories.index',compact(['inventories','userInventory','users','count']));
    }

    //Hàm chuyển hướng sang view add inventory
    public function add()
    {
        $users = DB::table('role_user')
                ->join('users','users.id','=','role_user.user_id')
                ->whereNotIn('role_user.role_id',[1,2])
                ->select('users.*')
                ->get();
        return view('inventories.add',compact('users'));
    }

    //Hàm create inventory
    public function postAdd(RequestCreateInventory $request)
    {
        try {
            DB::beginTransaction();
            $inventoryCreate = new Inventory;
            $inventoryCreate->name = $request->name;
            $inventoryCreate->address = $request->address;
            $inventoryCreate->max = $request->max;
            $inventoryCreate->the_rest = $request->max;
            $inventoryCreate->save();

            $userInventory = $request->managers;
            foreach ($userInventory as $uI) {
                \DB::table('user_inventory')->insert([
                    'inventory_id' => $inventoryCreate->id,
                    'user_id' => $uI,
                ]);
            }
            DB::commit();
            return redirect()->route('inventory');
        } catch (Exception $e) {
            DB::rollBack();
        }
        
    }

    //Hàm chuyển hướng sang view edit inventory
    public function edit($id)
    {
        $inventory = Inventory::findOrfail($id);
        $listManager = DB::table('user_inventory')->where('inventory_id',$id)->pluck('user_id');
        $users = User::where('id','<>',1)->get();
        return view('inventories.edit',compact(['inventory','users','listManager']));
    }

    //Hàm edit inventory
    public function postEdit($id, RequestEditInventory $request)
    {
        $inventory = DB::table('inventories')->where('id', '<>', $id)->get();
        foreach ($inventory as $i) {
            if($request->name == $i->name){
                $request->session()->flash('error', 'Inventory name already exists');
                return redirect()->back();
            }
        }

        try {
            DB::beginTransaction();
            $inventoryEdit = Inventory::find($id);
            if($request->max < $inventoryEdit->total && $inventoryEdit->total != 0){
                $request->session()->flash('error', 'Products in stock are now '.$inventoryEdit->total);
                return redirect()->back();
            }
            $inventoryEdit->max = $request->max;
            $inventoryEdit->the_rest = $inventoryEdit->max - $inventoryEdit->total;
            $inventoryEdit->name = $request->name;
            $inventoryEdit->address = $request->address;
            $inventoryEdit->save();

            DB::table('user_inventory')->where('inventory_id',$id)->delete();
            $userInventory = $request->managers;
            foreach ($userInventory as $uI) {
                \DB::table('user_inventory')->insert([
                    'inventory_id' => $inventoryEdit->id,
                    'user_id' => $uI,
                ]);
            }
        DB::commit();
        return redirect()->route('inventory');
        } catch (Exception $e) {
            DB::rollBack();
        }
    }

    //Hàm xóa inventory
    public function delete($id)
    {
        $this->authorize('Super admin');
        Inventory::find($id)->delete();

        $inventories = DB::table('inventories')->paginate(8);
        $userInventory = DB::table('user_inventory')->get();
        $users = User::all();
        $count = $inventories->count();
        return view('inventories.ajaxview',compact(['inventories','userInventory','users','count']));
    }
}
