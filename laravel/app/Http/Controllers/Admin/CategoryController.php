<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\RequestCategory;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Category;
use App\User;
use Auth;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::all();
        $count = $categories->count();
        return view('categories.index',compact(['categories','count']));
    }

    //Hàm chuyển hướng sang view add category
    public function add()
    {
        $category = Category::all();
        return view('categories.add',compact('category'));
    }

    //Hàm create category
    public function postAdd(RequestCategory $request)
    {
        Category::create($request->all());
        return redirect()->route('categories');
    }

    //Hàm chuyển hướng sang view edit category
    public function edit($id)
    {
        $category = Category::find($id);
        $categories = Category::all();
        return view('categories.edit',compact(['category','categories']));
    }

    //Hàm edit category
    public function postEdit($id, RequestCategory $request)
    {
        $categories = DB::table('categories')->where('id', '<>', $id)->get();
        foreach ($categories as $c) {
            if($request->name == $c->name){
                $request->session()->flash('error', 'Category name already exists');
                return redirect()->back();
            }
        }
        Category::where(['id' => $id])->update($request->only('name','parent_id'));
        return redirect()->route('categories');
    }

    //Hàm xóa category
    public function delete($id,Request $request)
    {
        $this->authorize('Super admin');
        $category = Category::find($id);
        $proCat = DB::table('products')
                ->join('categories', 'categories.id', '=', 'products.category_id')
                ->select('products.category_id','categories.parent_id')
                ->groupBy('products.category_id')
                ->get()->toArray();
        foreach ($proCat as $pC) {
            if ($category->id == $pC->parent_id) {
                $request->session()->flash('error', 'Category already have products !!!');
                return redirect()->back();
            }
            if ($pC->category_id == $id) {
                $request->session()->flash('error', 'Category already have products !!!');
                return redirect()->back();
            }
        }

        Category::find($id)->delete();
        Category::where('parent_id', $id)->delete();
        return redirect()->back();
    }
}
