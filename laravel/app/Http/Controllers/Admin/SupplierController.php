<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\RequestCreateSupplier;
use App\Http\Requests\RequestEditSupplier;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\User;
use App\Role;
use App\Inventory;
use App\Supplier;
use Auth;

class SupplierController extends Controller
{
    public function index()
    {
        $suppliers = Supplier::paginate(7);
        $count = $suppliers->count();
        return view('supplier.index',compact(['suppliers','count']));
    }

    //Hàm chuyển hướng sang view add supplier
    public function add()
    {
        return view('supplier.add');
    }

    // //Hàm create supplier
    public function postAdd(RequestCreateSupplier $request)
    {
        $supplier = new Supplier;
        $supplier->name = $request->name;
        $supplier->address = $request->address;
        $supplier->phone_number = $request->phone_number;
        $supplier->save();
        return redirect()->route('supplier');
    }

    //Hàm chuyển hướng sang view edit supplier
    public function edit($id)
    {
        $supplier = Supplier::findOrfail($id);
        return view('supplier.edit',compact(['supplier']));
    }

    //Hàm edit supplier
    public function postEdit($id, RequestEditSupplier $request)
    {
        $supplier = DB::table('suppliers')->where('id', '<>', $id)->get();
        foreach ($supplier as $s) {
            if($request->name == $s->name){
                $request->session()->flash('error', 'Supplier name already exists');
                return redirect()->back();
            }
            if($request->phone_number == $s->phone_number){
                $request->session()->flash('error', 'Phone number already exists');
                return redirect()->back();
            }
        }
        Supplier::where(['id' => $id])->update($request->only('name','address','phone_number'));
        return redirect()->route('supplier');
    }

    //Hàm xóa supplier
    public function delete($id,Request $request)
    {
        $this->authorize('Super admin');
        $supImport = DB::table('suppliers')
                ->join('import', 'import.supplier_id', '=', 'suppliers.id')
                ->select('import.supplier_id')
                ->groupBy('import.supplier_id')
                ->get()->toArray();
        foreach ($supImport as $sI) {
            if ($sI->supplier_id == $id) {
                $request->session()->flash('error', 'Supplier already have products !!!');
                return redirect()->back();
            }
        }

        Supplier::find($id)->delete();
        return redirect()->back();
    }
}
