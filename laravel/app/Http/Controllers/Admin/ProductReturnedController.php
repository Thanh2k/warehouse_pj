<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\RequestImportProReturned;
use App\Http\Requests\RequestEditProReturned;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Exports\ExcelProduct;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Inventory_product;
use App\Category;
use App\Inventory;
use App\Product;
use App\Import;
use App\Export;
use App\Supplier;
use App\Customer;
use App\User;
use File;
use Auth;
use Excel;

class ProductReturnedController extends Controller
{
    //Ham lay thong tin ds hàng bị trả về
    public function productReturned($invenid)
    {
        $proReturned = DB::table('import')
                ->join('products', 'products.id', '=', 'import.product_id')
                ->join('suppliers', 'suppliers.id', '=', 'import.supplier_id')
                ->crossJoin('users', 'users.id', '=', 'import.user_id')
                ->where([['import.inventory_id', '=', $invenid],['import.type', '=', 1]])
                ->select('import.*','products.name','users.username as username','suppliers.name as supname')
                ->paginate(6);
        $count = $proReturned->count();
        $customers = DB::table('customers')->get();
        $inventory = DB::table('inventories')
                    ->where('id','=',$invenid)
                    ->first();
        return view('products.proreturned',compact(['proReturned','invenid','customers','count','inventory']));
    }

    //Ham chyen huong sang view them san pham bi tra ve
    public function addProReturned($invenid)
    {
        $products = DB::table('products')
                ->join('inventory_product', 'inventory_product.product_id', '=', 'products.id')
                ->where('inventory_product.inventory_id', '=', $invenid)
                ->select('products.*')
                ->paginate(8);
        $customers = DB::table('customers')->get();
        $suppliers = DB::table('suppliers')->get();
        return view('products.addproreturned',compact(['products','customers','suppliers','invenid']));
    }

    //Ham them san pham bi tra ve
    public function postAddProReturned($invenid, RequestImportProReturned $request)
    {
        try {
            DB::beginTransaction();
            $inventory = Inventory::find($invenid);
            if($request->quantity_import > $inventory->max || ($request->quantity_import > $inventory->the_rest && $inventory->the_rest != 0)){
                $request->session()->flash('error', 'Inventory overflow, Maximum capacity of this repository is '.$inventory->max.', the rest capacity of this repository is '.$inventory->the_rest.' !!!');
                return redirect()->back();
            }
            $quantityProReturned = DB::table('import')
                    ->where([['inventory_id','=',$invenid],['product_id','=',$request->product_id],['type','=',1]])
                    ->select(DB::raw('sum(quantity_import) as sumimport'))
                    ->groupBy('product_id','inventory_id')
                    ->first();
            $export = DB::table('export')
                    ->where([['inventory_id','=',$invenid],['product_id','=',$request->product_id],['customer_id','=',$request->customer_return]])
                    ->get();
            $countExport = $export->count();
            if ($countExport < 1) {
                $request->session()->flash('error', 'The customer has not transactions !!!');
                return redirect()->back();
            }else{
                $totalProExport = 0;
                $totalPriceExport = 0;
                foreach ($export as $e) {
                    $totalProExport += $e->quantity_export;
                    $totalPriceExport += $e->price_export;
                }
            }
            if($request->price_import > ($totalPriceExport / $countExport) ){
                $request->session()->flash('error', 'Import price must be less than the average export price !!!');
                return redirect()->back();
            }
            if ($request->quantity_import > ($totalProExport - (isset($quantityProReturned) ? $quantityProReturned->sumimport : 0))) {
                $request->session()->flash('error', 'Your current number of products exceeds the quantity entered !!!');
                return redirect()->back();
            }
            $proImport = DB::table('import')
                    ->where([['inventory_id','=',$invenid],['product_id','=',$request->product_id],['supplier_id','=',$request->supplier_id]])
                    ->get();
            $countImport = $proImport->count();

            if ($countImport < 1) {
                $request->session()->flash('error', 'The Supplier does not provide this product !!!');
                return redirect()->back();
            }

            $import = new Import;
            $import->inventory_id = $invenid;
            $import->user_id = Auth::id();
            $import->product_id = $request->product_id;
            $import->supplier_id = $request->supplier_id;
            $import->quantity_import = $request->quantity_import;
            $import->price_product_return = $request->price_import;
            $import->type = 1;
            $import->reason_return = $request->reason_return;
            $import->customer_return = $request->customer_return;
            $import->created_at = Carbon::now()->format('Y-m-d');
            $import->updated_at = Carbon::now()->format('Y-m-d');
            $import->save();

            $inventoryProduct = DB::table('inventory_product')->where(['inventory_id' => $invenid,
                'product_id' => $request->product_id])->first();
            $realQuantity = ($inventoryProduct->real_quantity) + ($request->quantity_import);
            $request->merge(['real_quantity' => $realQuantity]);
            $inventoryProduct = Inventory_product::where(['inventory_id' => $invenid,
                'product_id' => $request->product_id])->update($request->only('real_quantity'));

            $totalProduct = DB::table('inventory_product')->where('inventory_id',$invenid)->sum('real_quantity');
            $inventory->total = $totalProduct;
            $inventory->the_rest = $inventory->the_rest - $request->quantity_import;
            $inventory->save();
            DB::commit();
            return redirect()->route('product_returned',['invenid'=>$invenid]);
        } catch (Exception $e) {
            DB::rollBack();
        }
    }

    //Ham chuyen sang view edit product returned
    public function editProReturned($invenid, $proid, $importid)
    {
        $import = DB::table('import')
                ->join('suppliers','suppliers.id','=','import.supplier_id')
                ->join('products','products.id','=','import.product_id')
                ->crossJoin('customers','customers.id','=','import.customer_return')
                ->select('import.*','suppliers.name','customers.name as cusname','products.name as proname')
                ->where('import.id',$importid)
                ->first();
        $customers = Customer::all();
        $suppliers = Supplier::all();
        return view('products.editproreturned',compact(['import','suppliers','customers','invenid','proid','importid']));   
    }

    //Hàm sửa thông tin product returned
    public function postEditProReturned($invenid, $proid, $importid, RequestEditProReturned $request)
    {
        try {
            DB::beginTransaction();
            $quantityProReturned = DB::table('import')
                    ->where([['inventory_id','=',$invenid],['product_id','=',$request->product_id],['type','=',1]])
                    ->select(DB::raw('sum(quantity_import) as sumimport'))
                    ->groupBy('product_id','inventory_id')
                    ->first();
            $export = DB::table('export')
                    ->where([['inventory_id','=',$invenid],['product_id','=',$request->product_id],['customer_id','=',$request->customer_return]])
                    ->get();
            $countExport = $export->count();
            if ($countExport < 1) {
                $request->session()->flash('error', 'The customer has not transactions !!!');
                return redirect()->back();
            }else{
                $totalProExport = 0;
                $totalPriceExport = 0;
                foreach ($export as $e) {
                    $totalProExport += $e->quantity_export;
                    $totalPriceExport += $e->price_export;
                }
            }
            if($request->price_import > ($totalPriceExport / $countExport) ){
                $request->session()->flash('error', 'Import price must be less than the average export price !!!');
                return redirect()->back();
            }
            $import = Import::where('id',$importid)->first();
            if ($request->quantity_import > ($totalProExport - ((isset($quantityProReturned) ? $quantityProReturned->sumimport : 0) - (isset($import) ? $import->quantity_import : 0)))) {
                $request->session()->flash('error', 'Your current number of products exceeds the quantity entered !!!');
                return redirect()->back();
            }
            $proImport = DB::table('import')
                    ->where([['inventory_id','=',$invenid],['product_id','=',$request->product_id],['supplier_id','=',$request->supplier_id]])
                    ->get();
            $countImport = $proImport->count();

            if ($countImport < 1) {
                $request->session()->flash('error', 'The Supplier does not provide this product !!!');
                return redirect()->back();
            }

            $editImport = Import::where('id',$importid)->first();
            $inventoryProduct = DB::table('inventory_product')->where(['inventory_id' => $invenid,
                'product_id' => $proid])->first();
            
            //Cập nhập lại số lượng thực trước khi update
            $quantityEx = $editImport->quantity_import - $inventoryProduct->real_quantity;
            $realQuantity = (($inventoryProduct->real_quantity) - ($editImport->quantity_import)) + ($request->quantity_import);
            $inventory = Inventory::find($invenid);
            if($realQuantity > $inventory->max || ($realQuantity > $inventory->the_rest && $inventory->the_rest != 0)){
                $request->session()->flash('error', 'Inventory overflow , Maximum capacity of this repository is '.$inventory->max.', the rest capacity of this repository is '.$inventory->the_rest.' !!!');
                return redirect()->back();
            }
            if ($request->quantity_import < $quantityEx) {
                $request->session()->flash('error', 'The amount to be imported must not be less than the amount exported !');
                return redirect()->back();
            }
            $request->merge(['real_quantity'=>$realQuantity]);
            $inventoryProduct = Inventory_product::where(['inventory_id' => $invenid,
                'product_id' => $proid])->update($request->only('real_quantity'));

            $editImport->supplier_id = $request->supplier_id;
            $editImport->customer_return = $request->customer_return;
            $editImport->price_product_return = $request->price_import;
            $editImport->quantity_import = $request->quantity_import;
            $editImport->updated_at = Carbon::now()->format('Y-m-d');
            $editImport->save();

            $totalProduct = DB::table('inventory_product')->where('inventory_id',$invenid)->sum('real_quantity');
            $inventoryUpdate = Inventory::find($invenid);
            $inventoryUpdate->total = $totalProduct;
            $inventoryUpdate->the_rest = $inventoryUpdate->max - $totalProduct;
            $inventoryUpdate->save();

            $proReturned = DB::table('import')
                ->join('products', 'products.id', '=', 'import.product_id')
                ->join('suppliers', 'suppliers.id', '=', 'import.supplier_id')
                ->crossJoin('users', 'users.id', '=', 'import.user_id')
                ->where([['import.inventory_id', '=', $invenid],['import.type', '=', 1]])
                ->select('import.*','products.name','users.username as username','suppliers.name as supname')
                ->orderBy('import.id', 'desc')
                ->paginate(6);
            $count = $proReturned->count();
            $customers = DB::table('customers')->get();
            DB::commit();
            return view('products.proreturned',compact(['proReturned','invenid','customers','count']));
        } catch (Exception $e) {
            DB::rollBack();
        }
    }

    //Hàm xóa product returned
    public function deleteProReturned($invenid, $proid, $importid, Request $request)
    {
        $import = Import::where('id',$importid)->first();
        $inventoryProduct = DB::table('inventory_product')->where(['inventory_id' => $invenid,
                'product_id' => $proid])->first();
        if ($import->quantity_import > $inventoryProduct->real_quantity) {
            $request->session()->flash('error', 'Current quantity in stock is '.$inventoryProduct->real_quantity.' but the number of products you want to delete is '.$import->quantity_import.' !');
            return redirect()->back();
        }

        $realQuantity = ($inventoryProduct->real_quantity) - ($import->quantity_import);
        $request->merge(['real_quantity' => $realQuantity]);
        $inventoryProduct = Inventory_product::where(['inventory_id' => $invenid,
                'product_id' => $proid])->update($request->only('real_quantity'));
        $totalProduct = DB::table('inventory_product')->where('inventory_id',$invenid)->sum('real_quantity');
        $inventory = Inventory::find($invenid);
        $inventory->total = $totalProduct;
        $inventory->the_rest = $inventory->max - $totalProduct;
        $inventory->save();
        Import::where('id',$importid)->delete();
        return redirect()->back();
    }

    //Ham lay ra top 3 san pham bi tra ve nhieu nhat
    public function getTopProReturned($invenid)
    {
        $import =  DB::table('products')
                ->join('import', 'import.product_id', '=', 'products.id')
                ->select('import.*','products.*',DB::raw('sum(import.quantity_import) as sumimport'))
                ->where([['import.inventory_id', '=', $invenid],['import.type', '=', 1]])
                ->groupBy('import.product_id','import.inventory_id')
                ->orderBy('sumimport', 'desc')
                ->limit(3)
                ->get();

        $inventory = Inventory::where('id',$invenid)->first();
        return view('products.topproreturned',compact(['invenid','import','inventory']));
    }
}
