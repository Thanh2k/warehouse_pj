<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = 'customers';

    protected $fillable = [
        'name', 'email', 'phone_number', 'address', 'created_at','updated_at'
    ];

    public function scopeKey($query, $request)
    {
        if ($request->has('key')) {
            $query
            ->where([
                ['name', 'LIKE', '%' . $request->key . '%'],
            ])
            ->orWhere([
                ['email', 'LIKE', '%' . $request->key . '%'],
            ])
            ->orWhere([
                ['address', 'LIKE', '%' . $request->key . '%'],
            ])
            ->orWhere([
                ['phone_number', '=',$request->key],
            ]);
        }

        return $query;
    }
}
