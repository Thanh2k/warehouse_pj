<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    protected $table = 'inventories';

    protected $fillable = [
        'id','address', 'quantity', 'name', 'max', 'the_rest', 'created_at','updated_at'
    ];

    public function user()
    {
        return $this->belongsToMany('App\User','user_inventory','user_id','inventory_id');
    }

    public function product()
    {
        return $this->belongsToMany('App\Product', 'inventory_product');
    }
}
