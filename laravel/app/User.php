<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Auth;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'id','full_name', 'email', 'password', 'username', 'fist_login_status', 'level', 'address','birthday', 'phone_number','created_at','updated_at'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function role()
    {
        return $this->belongsToMany('App\Role','role_user', 'user_id', 'role_id');
    }

    public function inventory()
    {
        return $this->belongsToMany('App\Inventory','user_inventory','user_id','inventory_id');
    }

    public function hasRoles(Role $roles)
    {
        $user = User::find(Auth::id());
        return !! optional($user->role)->contains($roles);
    }
}
