<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Product extends Model
{
    public $timestamps = true;

    protected $fillable = [
        'image', 'category_id', 'unit_id', 'name','quantity','price','created_at','updated_at'
    ];

    public function inventory()
    {
        return $this->belongsToMany('App\Inventory', 'inventory_product');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function unit()
    {
        return $this->belongsTo('App\Unit');
    }

    public function import()
    {
        return $this->hasMany('App\Import');
    }

    public function export()
    {
        return $this->hasMany('App\Export');
    }

    public function scopeKey($query, $request)
    {
        $invenid = $request->invenid;
        if ($request->has('key')) {
            $query->join('inventory_product','inventory_product.product_id','=','products.id')
            ->join('categories', 'categories.id', '=', 'products.category_id')
            ->join('units', 'units.id', '=', 'products.unit_id')
            ->join('import', 'import.product_id', '=', 'products.id')
            ->crossJoin('suppliers', 'suppliers.id', '=', 'import.supplier_id')
            ->where([
                ['inventory_product.inventory_id', '=', $invenid],
                ['products.name', 'LIKE', '%' . $request->key . '%'],
            ])
            ->orWhere([
                ['inventory_product.inventory_id', '=', $invenid],
                ['categories.name', 'LIKE', '%' . $request->key . '%'],
            ])
            ->orWhere([
                ['inventory_product.inventory_id', '=', $invenid],
                ['suppliers.name', 'LIKE', '%' . $request->key . '%'],
            ])
            ->orWhere([
                ['inventory_product.inventory_id', '=', $invenid],
                ['units.name', 'LIKE', '%' . $request->key . '%'],
            ])
            ->select('inventory_product.*','products.*','categories.name as catname','units.name as unitname','suppliers.name as supname')
            ->groupBy('products.id','inventory_product.id');
        }

        return $query;
    }
}
