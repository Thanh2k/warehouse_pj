<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Export extends Model
{
    protected $table = 'export';

    protected $fillable = [
        'user_id', 'inventory_id', 'product_id', 'customer_id', 'quantity_export', 'price_export', 'status', 'created_at','updated_at'
    ];

    public function scopeKey($query, $request)
	{
		$invenid = $request->invenid;
	    if ($request->has('key')) {
            $query->join('products', 'products.id', '=', 'export.product_id')
            ->join('customers', 'customers.id', '=', 'export.customer_id')
            ->crossJoin('users', 'users.id', '=', 'export.user_id')
            ->where([
                ['export.inventory_id', '=', $invenid],
                ['export.quantity_export', '=', $request->key],
            ])
            ->orWhere([
                ['export.inventory_id', '=', $invenid],
                ['export.price_export', '=', $request->key],
            ])
            ->orWhere([
                ['export.inventory_id', '=', $invenid],
                ['products.name', 'LIKE', '%' . $request->key . '%'],
            ])
            ->orWhere([
                ['export.inventory_id', '=', $invenid],
                ['users.username', 'LIKE', '%' . $request->key . '%'],
            ])
            ->orWhere([
                ['export.inventory_id', '=', $invenid],
                ['customers.name', 'LIKE', '%' . $request->key . '%'],
            ])
            ->select('export.*','products.name as proname','customers.name as cusname','users.username as username')
            ->orderBy('export.id', 'desc');
        }

	    return $query;
	}

	public function scopeFrom($query, $request)
	{
		$invenid = $request->invenid;
	    if ($request->from != null && $request->to == null) {
            $query->join('products', 'products.id', '=', 'export.product_id')
            ->join('customers', 'customers.id', '=', 'export.customer_id')
            ->crossJoin('users', 'users.id', '=', 'export.user_id')
            ->where([
                ['export.inventory_id', '=', $invenid],
                ['export.created_at', '=', date('Y-m-d',strtotime($request->from))],
            ])
            ->select('export.*','products.name as proname','customers.name as cusname','users.username as username')
            ->orderBy('export.id', 'desc');
        }

	    return $query;
	}

	public function scopeTo($query, $request)
	{
		$invenid = $request->invenid;
	    if ($request->to != null && $request->from == null) {
            $query->join('products', 'products.id', '=', 'export.product_id')
            ->join('customers', 'customers.id', '=', 'export.customer_id')
            ->crossJoin('users', 'users.id', '=', 'export.user_id')
            ->where([
                ['export.inventory_id', '=', $invenid],
                ['export.created_at', '=', date('Y-m-d',strtotime($request->to))],
            ])
            ->select('export.*','products.name as proname','customers.name as cusname','users.username as username')
            ->orderBy('export.id', 'desc');
        }

	    return $query;
	}

	public function scopeFromTo($query, $request)
	{
		$invenid = $request->invenid;
	    if ($request->from != null && $request->to != null) {
             $query->join('products', 'products.id', '=', 'export.product_id')
            ->join('customers', 'customers.id', '=', 'export.customer_id')
            ->crossJoin('users', 'users.id', '=', 'export.user_id')
            ->where([
                ['export.inventory_id', '=', $invenid],
                ['export.created_at', '>=', date('Y-m-d',strtotime($request->from))],
                ['export.created_at', '<=', date('Y-m-d',strtotime($request->to))],
            ])
            ->select('export.*','products.name as proname','customers.name as cusname','users.username as username')
            ->orderBy('export.id', 'desc');
        }

	    return $query;
	}
}
