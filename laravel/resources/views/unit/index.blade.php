@extends('adminlte::page')
@section('title', 'Units')
@section('content_header')
    <h1 style="margin-left: 35%;color: #22a7a7;">Units</h1>
@stop
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div id="error" style="display: none">{{session('error')}}</div>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Stt</th>
                        <th style="width: 25%;">Name</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php $stt = 1; ?>
                    @foreach($units as $u)
                    <tr>
                        <td>{{$stt++}}</td>
                        <td>{{$u->name}}</td>
                        <td>
                            <a href="{{ route('unit_edit',['id'=>$u->id]) }}" class="btn btn-sm btn-primary">Edit</a>
                            <a href="{{ route('unit_delete',['id'=>$u->id]) }}" class="btn btn-sm btn-danger" onclick="return confirm('If you delete this unit, you may lose the system history. Are you sure ?')">Delete</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @if($count < 1)
                <h3 style="text-align: center;">You do not have any unit !!!</h3>
            @endif
            <div style="margin-left: 88%;">
               <a href="{{ route('unit_add') }}" class="btn btn-sm btn-success">Add unit</a>
            </div>
            <div class="pull-right">
                {{ $units->links('vendor.pagination.default') }}
            </div>
        </div>
    </div>
</div>
@endsection
@section('css')
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/dist/css/table.css') }}">
@stop

@section('js')
    <script>
        if($("#error").text() != ""){
            w2popup.open({
                title   : 'ERROR',
                body    : $("#error").text(),
                width: 450,
                height: 90,
            });
        }
        
    </script>
@stop
