@extends('adminlte::page')
@section('title', 'Edit import')
@section('content_header')
    <h1 style="margin-left: 12%;color: #49498e;padding-bottom: 1%;">Edit import product {{$product->name}}</h1>
@stop

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                	<div id="error" style="display: none">{{session('error')}}</div>
                    <form method="POST" action="{{ route('import_edit',['invenid'=>$invenid,'proid'=>$proid,'importid'=>$importid]) }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Product name</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" disabled name="name" value="{{$product->name}}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Quantity import</label>

                            <div class="col-md-6">
                                <input type="number" class="form-control" name="quantity_import" value="{{$import->quantity_import}}">
                                <div style="color: red;">
                                    @if($errors->has('quantity_import'))
                                        {{ $errors->first('quantity_import') }}
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Price import</label>

                            <div class="col-md-6">
                                <input type="number" class="form-control" step="0.01" name="price_import" value="{{$import->price_import}}">
                                <div style="color: red;">
                                    @if($errors->has('price_import'))
                                        {{ $errors->first('price_import') }}
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0" style="margin-left: 16%;padding-top: 2% !important;">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Update
                                </button>
                                <a href="{{route('importdetail',['invenid'=>$invenid])}}" class="btn btn-warning" style="margin-left: 8%;">Back</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('css')
    <link rel="stylesheet" href="">
@stop
@section('js')
    <script>
        if($("#error").text() != ""){
            w2popup.open({
                title   : 'ERROR',
                body    : $("#error").text(),
                width: 450,
                height: 90,
            });
        }
        
    </script>
@stop
