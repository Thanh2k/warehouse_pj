@extends('adminlte::page')
@section('title', 'Your Inventory')
@section('content_header')
@stop

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <?php $stt = 1; $flag = true; ?>
            @foreach($userId as $uI)
                @if($userCurrent == $uI->id)
                    <?php $flag = false; ?>
                @endif  
            @endforeach
            @if($flag == false)
                @if($countAdminInven < 1)
                    <h3 style="text-align: center;color: #49498e;">You do not have any inventory !!!</h3>
                @endif
                @foreach($adminInven as $i)
                    <div class="list-group-item list-group-item-info inventory animated jackInTheBox">
                        <h2 title="{{$i->name}}">{{$i->name}}</h2>
                        <a href="{{ route('product',['invenid'=>$i->id]) }}" class="btn btn-sm btn-success detail">Detail</a>
                        <a href="{{ route('product_returned',['invenid'=>$i->id]) }}" class="btn btn-sm btn-primary proreturned">Product returned</a>
                        <a href="{{ route('importdetail',['invenid'=>$i->id]) }}" class="btn btn-sm btn-success importdetail">Import Detail</a>
                        <a href="{{ route('exportdetail',['invenid'=>$i->id]) }}" class="btn btn-sm btn-danger exportdetail">Export Detail</a>
                        <div class="div-chart"><a href="{{ route('chart',['invenid'=>$i->id]) }}" class="btn btn-sm btn-success chart">Chart</a></div>
                    </div>
                @endforeach
                <div class="pull-right" style="position: fixed;bottom: 0;right: 2%;">
                    {{ $adminInven->links() }}
                </div>
            @endif    
            @if($flag == true)    
            @if($countInven < 1)
                <h3 style="text-align: center;color: #49498e;">You do not have any inventory !!!</h3>
            @endif
                @foreach($inventories as $i)
                    <div class="list-group-item list-group-item-info inventory animated jackInTheBox">
                        <h2 title="{{$i->name}}">{{$i->name}}</h2>
                        <a href="{{ route('product',['invenid'=>$i->id]) }}" class="btn btn-sm btn-success detail">Detail</a>
                        <a href="{{ route('product_returned',['invenid'=>$i->id]) }}" class="btn btn-sm btn-primary proreturned">Product returned</a>
                        <a href="{{ route('importdetail',['invenid'=>$i->id]) }}" class="btn btn-sm btn-success importdetail">Import Detail</a>
                        <a href="{{ route('exportdetail',['invenid'=>$i->id]) }}" class="btn btn-sm btn-danger exportdetail">Export Detail</a>
                        <div class="div-chart"><a href="{{ route('chart',['invenid'=>$i->id]) }}" class="btn btn-sm btn-success chart">Chart</a></div>
                    </div>
                @endforeach
                <div class="pull-right" style="position: fixed;bottom: 0;right: 2%;">
                    {{ $inventories->links('vendor.pagination.default') }}
                </div>
            @endif
        </div>
    </div>
</div>
@endsection
@section('css')
<link rel="stylesheet" href="{{ asset('vendor/adminlte/dist/css/homestyle.css') }}">
@stop

@section('js')
<script> console.log('Hi!'); </script>
@stop