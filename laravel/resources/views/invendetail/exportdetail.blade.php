@extends('adminlte::page')
@section('title', 'Export Detail')
@section('content_header')
<div class="text-center" style="margin-right: 7%;">
    <h3 style="color: red;margin-top: 0;">Export Detail Inventory {{$inventory->name}}</h3>
    <h4>Total: {{$countExport}} time(s) export</h4>
    <h6 >{{$quantityExport->sumexport}} product(s)</h6>
</div>
@stop

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="col-md-6" style="margin-bottom: 2%;">
                <form class="form-inline" action="{{ route('filterEx') }}" method="GET">
                    <div>
                        <input class="form-control" style="width: 160px;" name="key" type="text" placeholder="Search" aria-label="Search">
                        <input type="hidden" name="invenid" value="{{$invenid}}">
                        <button type="submit" class="btn btn-primary"><i class="fas fa-search" aria-hidden="true"></i></button>
                    </div>
                </form>
            </div>
            <div class="col-md-5" style="margin-bottom: 2%;margin-left: 8%">
                <form class="form-inline" action="{{ route('filterEx') }}" method="GET">
                    <div>
                        <input class="form-control" style="width: 155px;" name="from" type="date" placeholder="Search" aria-label="Search">
                        <input class="form-control" style="width: 155px;" name="to" type="date" placeholder="To" aria-label="Search">
                        <input type="hidden" name="invenid" value="{{$invenid}}">
                        <button type="submit" class="btn btn-primary"><i class="fas fa-search" aria-hidden="true"></i></button>
                    </div>
                </form>
            </div>
            
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Stt</th>
                        <th>Product name</th>
                        <th>User Export</th>
                        <th>Customer</th>
                        <th>Quantity Export</th>
                        <th>Price Export</th>
                        <th>Export time</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php $stt = 1; ?>
                    @foreach($export as $e)
                          <tr>
                              <td>{{$stt++}}</td>
                              <td>{{$e->proname}}</td>
                              <td>
                                {{$e->username}}
                             </td>
                             <td>
                                {{$e->cusname}}
                             </td>
                             <td>{{$e->quantity_export}}</td>
                             <td><?php echo number_format($e->price_export,2); ?> $</td>
                             <td>{{date('d-m-Y', strtotime($e->created_at))}}</td> 
                             <td>
                                <a href="{{ route('export_edit',['invenid'=>$invenid,'proid'=>$e->product_id,'exportid'=>$e->id]) }}" class="btn btn-sm btn-warning">Edit</a>
                             </td>
                         </tr>
                   @endforeach
               </tbody>
           </table>
            @if($countExport < 1)
                <h3 style="text-align: center;">Inventory do not have any export detail !!!</h3>
            @endif
           <div style="margin-left: 64%">
               <a href="{{ route('export_excel',['invenid' => $invenid]) }}" class="btn btn-sm btn-primary">Export excel</a>
               <a href="{{ route('discharge_goods',['invenid' => $invenid]) }}" class="btn btn-sm btn-danger">Discharge of goods</a>
               <a href="{{ route('top_selling',['invenid' => $invenid]) }}" class="btn btn-sm btn-info">Top Selling</a>
               <a href="{{ route('invendetail') }}" class="btn btn-sm btn-success">Back</a>
           </div>
            <div class="pull-right">
                {{ $export->links('vendor.pagination.default') }}
            </div>
       </div>
    </div>
</div>
@endsection
@section('css')
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/dist/css/table.css') }}">
@stop

@section('js')
<!-- <script>
    $("#datepicker").datetimepicker({
        format:'m/d/Y',
        timepicker:false,
        pick:null,
    });
</script> -->
@stop