@extends('adminlte::page')
@section('title', 'Top Selling')
@section('content_header')
<h1 style="margin-left: 30%;color: red;">Top Selling Inventory {{$inventory->name}}</h1>
@stop

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Stt</th>
                        <th>Product name</th>
                        <th>Total Quantity Export</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $stt = 1; $flag = false; ?>
                    @foreach($export as $e)
                    <?php $flag = true; ?>
                      <tr>
                          <td>{{$stt++}}</td>
                          <td>{{$e->name}}</td>
                          <td>{{$e->sumexport}}</td>
                      </tr>
                   @endforeach
               </tbody>
           </table>
           <div class="text-center">
             @if($flag == false)
              <h3>You do not have any product !</h3>
             @endif
           </div>
           <a href="{{route('exportdetail',['invenid'=>$invenid])}}" class="btn btn-sm btn-success" style="margin-left: 94%;">Back</a>
       </div>
    </div>
</div>
@endsection
@section('css')
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/dist/css/table.css') }}">
@stop

@section('js')
<!-- <script>
    $("#datepicker").datetimepicker({
        format:'m/d/Y',
        timepicker:false,
        pick:null,
    });
</script> -->
@stop