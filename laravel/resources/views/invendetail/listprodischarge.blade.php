@extends('adminlte::page')
@section('title', 'List Product Discharge')
@section('content_header')
<h1 style="margin-left: 30%;color: red;">List Product Discharge {{$inventory->name}}</h1>
@stop

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Stt</th>
                        <th>Product name</th>
                        <th>Price Import</th>
                        <th>Price Export</th>
                        <th>Total Quantity</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $stt = 1; $flag = false; ?>
                    @foreach($listDischarge as $lD)
                    <?php $flag = true; ?>
                      <tr>
                          <td>{{$stt++}}</td>
                          <td>{{$lD->name}}</td>
                          <td><?php echo number_format($lD->newest_price_import,2); ?> $</td>
                          <td><?php echo number_format($lD->price_export,2); ?> $</td>
                          <td>{{$lD->sumexport}}</td>
                      </tr>
                   @endforeach
               </tbody>
           </table>
           <div class="text-center">
             @if($flag == false)
              <h3>You do not have any product !</h3>
             @endif
           </div>
           <a href="{{route('exportdetail',['invenid'=>$invenid])}}" class="btn btn-sm btn-success" style="margin-left: 93%;">Back</a>
       </div>
       <div class="pull-right" style="position: fixed;bottom: 0;right: 2%;">
            {{ $listDischarge->links('vendor.pagination.default') }}
        </div>
    </div>
</div>
@endsection
@section('css')
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/dist/css/table.css') }}">
@stop

@section('js')
<!-- <script>
    $("#datepicker").datetimepicker({
        format:'m/d/Y',
        timepicker:false,
        pick:null,
    });
</script> -->
@stop