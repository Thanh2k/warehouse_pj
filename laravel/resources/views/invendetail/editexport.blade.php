@extends('adminlte::page')
@section('title', 'Edit export')
@section('content_header')
    <h1 style="margin-left: 10%;color: #49498e;padding-bottom: 1%;">Edit export product {{$product->name}}</h1>
@stop

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                	<div id="error" style="display: none">{{session('error')}}</div>
                    <form method="POST" action="">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Product name</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" disabled name="name" value="{{$product->name}}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Customer</label>

                            <div class="col-md-6">
                                <select name="customer_id" class="form-control">
                                    @foreach($customers as $c)
                                        <option value="{{$c->id}}" {{($export->customer_id == $c->id) ? 'selected' : ''}}>{{$c->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Quantity export</label>

                            <div class="col-md-6">
                                <input id="name" type="number" class="form-control" name="quantity_export" value="{{$export->quantity_export}}">
                                <div style="color: red;">
                                    @if($errors->has('quantity_export'))
                                        {{ $errors->first('quantity_export') }}
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Price export</label>

                            <div class="col-md-6">
                                <input id="name" type="number" class="form-control" name="price_export" step="0.01" value="{{$export->price_export}}">
                                <div style="color: red;">
                                    @if($errors->has('price_export'))
                                        {{ $errors->first('price_export') }}
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0" style="margin-left: 15%;padding-top: 2% !important;">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Update
                                </button>
                                <a href="{{route('exportdetail',['invenid'=>$invenid])}}" class="btn btn-warning" style="margin-left: 8%;">Back</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('css')
    <link rel="stylesheet" href="">
@stop
@section('js')
     <script>
        if($("#error").text() != ""){
            w2popup.open({
                title   : 'ERROR',
                body    : $("#error").text(),
                width: 450,
                height: 90,
            });
        }
        
    </script>
@stop
