@extends('adminlte::page')
@section('title', 'Chart')
@section('content_header')
@stop

@section('content')
    <div class="row">
        <h2 class="text-center animated bounceInDown" style="color: red;">Chart Inventory {{$inventory->name}}</h2>
        <div class="col-md-6 animated bounceInRight" style="margin-top: 2%;margin-left: 76%;">
            <form action="{{ route('filter_chart',['invenid'=>$invenid]) }}" method="GET">
                <div>
                    <input class="form-control" style="width: 120px;" name="key" type="number" placeholder="Year" aria-label="Year" autocomplete="off">
                </div>
            </form>
        </div>
        <div class="col-md-11" style="margin-left: 2.5%;margin-top: 2%;">
            {!! $chart->container() !!}
        </div>
        <div class="col-md-11" style="margin-left: 2.5%;margin-top: 2%;">
            {!! $interestChart->container() !!}
        </div>
    </div>
    <a href="{{ route('invendetail') }}" style="margin-left: 89%;margin-top: 3%;" class="btn btn-sm btn-primary">Back</a>
@endsection
@section('css')
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/dist/css/homestyle.css') }}">
    <style>
        .form-control{
            border-top: none;
            border-left: none;
            border-right: none;
            border-bottom: #00f6ff solid 1px;
        }
        .content-header {
            padding: 0;
        }
    </style>
@stop

@section('js')
<script> console.log('Hi!'); </script>
{!! $chart->script() !!}
{!! $interestChart->script() !!}
@stop