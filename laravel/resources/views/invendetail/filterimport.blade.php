@extends('adminlte::page')
@section('title', 'Import Detail')
@section('content_header')
<div class="text-center" style="margin-right: 7%;">
    <h3 style="color: red;margin-top: 0;">Import Detail Inventory {{$inventory->name}}</h3>
    <h4>Find: {{$countImport}} record(s)</h4>
</div>
@stop

@section('content')
<div class="container">
   <div class="row justify-content-center">
    <div class="col-md-10">
        <div class="col-md-6" style="margin-bottom: 2%;">
            <form class="form-inline" action="{{ route('filterIm') }}" method="GET">
                <div>
                    <input class="form-control" style="width: 160px;" name="key" type="text" placeholder="Search" aria-label="Search">
                    <input type="hidden" name="invenid" value="{{$invenid}}">
                    <button type="submit" class="btn btn-primary"><i class="fas fa-search" aria-hidden="true"></i></button>
                </div>
            </form>
        </div>
        <div class="col-md-5" style="margin-bottom: 2%;margin-left: 8%;">
            <form class="form-inline" action="{{ route('filterIm') }}" method="GET">
                <div>
                    <input class="form-control" style="width: 155px;" name="from" type="date" placeholder="Search" aria-label="Search">
                    <input class="form-control" style="width: 155px;" name="to" type="date" placeholder="To" aria-label="Search">
                    <input type="hidden" name="invenid" value="{{$invenid}}">
                    <button type="submit" class="btn btn-primary"><i class="fas fa-search" aria-hidden="true"></i></button>
                </div>
            </form>
        </div>
        <table class="table table-hover">
           <thead>
                <tr>
                    <th>Stt</th>
                    <th>Product name</th>
                    <th>User Import</th>
                    <th>Supplier</th>
                    <th>Type</th>
                    <th>Quantity Import</th>
                    <th>Price Import</th>
                    <th>Import date</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php $stt = 1; ?>
                @foreach($import as $i)
                <tr>
                    <td>{{$stt++}}</td>
                    <td>{{$i->name}}</td>
                    <td>{{$i->username}}</td>
                    <td>{{$i->supname}}</td>
                    <td><span class="label label-{{($i->type == 1) ? 'danger' : 'success'}}">{{($i->type == 1) ? 'Returned' : 'New'}}</span></td>
                    <td style="width: 8%;">{{$i->quantity_import}}</td>
                    <td>
                        @if($i->type == 1) 
                            <?php echo number_format($i->price_product_return,2); ?> $
                        @else
                            <?php echo number_format($i->price_import,2); ?>    $
                        @endif
                    </td>
                    <td>{{date('d-m-Y', strtotime($i->created_at))}}</td> 
                    <td>
                        <a href="{{ route('import_edit',['invenid'=>$invenid,'proid'=>$i->product_id,'importid'=>$i->id]) }}" class="btn btn-sm btn-warning {{($i->type == 1 ? 'disabled' : '')}}">Edit</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <a href="{{ route('import_filter_excel',[
           'invenid' => $_GET['invenid'],
           'key' => isset($_GET['key']) ? $_GET['key'] : 'z',
           'from' => !empty($_GET['from']) ? $_GET['from'] : 'z',
           'to' => !empty($_GET['to']) ? $_GET['to'] : 'z',
        ]) }}" style="margin-left: 85%" class="btn btn-sm btn-info">Export excel</a>
        <a href="{{route('invendetail')}}" class="btn btn-sm btn-success">Back</a>
    </div>
</div>
</div>
@endsection
@section('css')
<link rel="stylesheet" href="{{ asset('vendor/adminlte/dist/css/table.css') }}">
@stop

@section('js')
<script> console.log('Hi!'); </script>
@stop