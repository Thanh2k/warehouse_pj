@extends('adminlte::page')
@section('title', 'Customers')
@section('content_header')
    <h1 style="margin-left: 35%;color: #22a7a7;">Customers</h1>
    <h4 style="margin-left: 33%;color: ">Find : {{$countCus}} customer(s)</h4>
@stop
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div id="error" style="display: none">{{session('error')}}</div>
            <div class="col-md-6" style="margin-bottom: 2%;margin-left: 76%;">
                <form class="form-inline" action="{{ route('filter_customer') }}" method="GET">
                    <div>
                        <input class="form-control" style="width: 165px;" name="key" type="text" placeholder="Search" aria-label="Search">
                        <button type="submit" class="btn btn-primary"><i class="fas fa-search" aria-hidden="true"></i></button>
                    </div>
                </form>
            </div>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Stt</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Address</th>
                        <th>Phone number</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php $stt = 1; ?>
                    @foreach($customers as $c)
                    <tr>
                        <td>{{$stt++}}</td>
                        <td>{{$c->name}}</td>
                        <td>{{$c->email}}</td>
                        <td>{{$c->address}}</td>
                        <td>{{$c->phone_number}}</td>
                        <td>
                            <a href="{{ route('customer_edit',['id'=>$c->id]) }}" class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-edit"></span></a>
                            <a href="{{ route('customer_delete',['id'=>$c->id]) }}" class="btn btn-sm btn-danger" onclick="return confirm('If you delete this customer, you may lose the system history. Are you sure ?')"><span class="glyphicon glyphicon-trash"></span></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div style="margin-left: 82%;">
               <a href="{{ route('customer_add') }}" class="btn btn-sm btn-success">Add customer</a>
               <a href="{{ route('customer') }}" class="btn btn-sm btn-warning" style="margin-left: 1%;">Back</a>
            </div>
            <div class="pull-right">
                {{ $customers->links('vendor.pagination.default') }}
            </div>
        </div>
    </div>
</div>
@endsection
@section('css')
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/dist/css/table.css') }}">
@stop

@section('js')
    <script>
        if($("#error").text() != ""){
            w2popup.open({
                title   : 'ERROR',
                body    : $("#error").text(),
                width: 450,
                height: 90,
            });
        }
        
    </script>
@stop
