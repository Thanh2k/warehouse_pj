@extends('adminlte::page')
@section('title', 'Edit Customer')
@section('content_header')
    <h1 style="margin-left: 15%;color: #49498e;padding-bottom: 1%;">Edit Customer {{$customer->name}}</h1>
@stop

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <div id="error" style="display: none">{{session('error')}}</div>
                    <form method="POST" action="">
                        @csrf
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Name <small>(*)</small></label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{$customer->name}}">
                                <div style="color: red;">
                                    @if($errors->has('name'))
                                        {{ $errors->first('name') }}
                                    @endif
                                </div>
                            </div>
                        </div>

                       <div class="form-group row">
                            <label for="" class="col-md-4 col-form-label text-md-right">Email <small>(*)</small></label>

                            <div class="col-md-6">
                                <input id="" type="email" class="form-control" name="email" value="{{$customer->email}}">
                                <div style="color: red;">
                                    @if($errors->has('email'))
                                        {{ $errors->first('email') }}
                                    @endif
                                </div>
                            </div>
                        </div>

                       <div class="form-group row">
                            <label for="" class="col-md-4 col-form-label text-md-right">Address <small>(*)</small></label>

                            <div class="col-md-6">
                                <input id="" type="text" class="form-control" name="address" value="{{$customer->address}}">
                                <div style="color: red;">
                                    @if($errors->has('address'))
                                        {{ $errors->first('address') }}
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">Phone number <small>(*)</small></label>

                            <div class="col-md-6">
                                <input id="" type="text" class="form-control" name="phone_number" value="{{$customer->phone_number}}">
                                <div style="color: red;">
                                    @if($errors->has('phone_number'))
                                        {{ $errors->first('phone_number') }}
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0" style="margin-left: 16%;padding-top: 2% !important;">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-sm btn-primary">
                                    Update
                                </button>
                                <a href="{{ route('customer') }}" class="btn btn-sm btn-warning" style="margin-left: 10%;">Back</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('css')
    <link rel="stylesheet" href="">
@stop

@section('js')
    <script>
        if($("#error").text() != ""){
            w2popup.open({
                title   : 'ERROR',
                body    : $("#error").text(),
                width: 450,
                height: 90,
            });
        }
        
    </script>
@stop