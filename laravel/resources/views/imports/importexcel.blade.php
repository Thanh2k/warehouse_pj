@extends('adminlte::page')
@section('title', 'Import data')
@section('content_header')
    <h1 style="margin-left: 15%;color: #49498e;padding-bottom: 1%;">Import data by excel file</h1>
@stop

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                   <div id="error" style="display: none">{{session('error')}}</div>
                   <div id="success" style="display: none">{{session('success')}}</div>
                    <form action="{{route('import_by_excel',['invenid' => $invenid])}}" method="POST" enctype="multipart/form-data">
			            @csrf
			            <input required type="file" name="import_file">
                        <div style="color: red;padding-top: 2px;">
                            @if($errors->has('import_file'))
                                {{ $errors->first('import_file') }}
                            @endif
                        </div>
			            <br>
			            <a href="{{route('template_import')}}" class="btn btn-sm btn-primary">Download Template Import Data</a>
                        <button type="submit" class="btn btn-sm btn-success"  style="margin-left: 2%;">
                            Import by excel file
                        </button>
                        <a href="{{route('product',['invenid'=>$invenid])}}" class="btn btn-sm btn-warning" style="margin-left: 2%;">Back</a>
			        </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('css')
    <link rel="stylesheet" href="">
@stop

@section('js')
    <script>
        if($("#error").text() != ""){
            w2popup.open({
                title   : 'ERROR',
                body    : $("#error").text(),
                width: 450,
                height: 90,
            });
        }
        if($("#success").text() != ""){
            w2popup.open({
                title   : 'SUCCESS',
                body    : $("#success").text(),
                width: 450,
                height: 90,
            });
        }
        
    </script>
@stop