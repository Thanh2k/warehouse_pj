@extends('adminlte::page')
@section('title', 'List manager inventory')
@section('content_header')
    <h1 style="margin-left: 30%;color: #49498e;">List manager inventory</h1>
@stop

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div id="error" style="display: none">{{session('error')}}</div>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>STT</th>
                        <th>Username</th>
                        <th>Fullname</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Birthday</th>
                        <th>Position</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php $stt = 1; ?>
                    @foreach($users as $u)
                    <tr>
                        <td>{{$stt++}}</td>
                        <td>{{$u->username}}</td>
                        <td>{{(!empty($u->full_name)) ? $u->full_name : 'Not updated'}}</td>
                        <td>{{(!empty($u->email)) ? $u->email : 'Not updated'}}</td>
                        <td>{{(!empty($u->phone_number)) ? $u->phone_number : 'Not updated'}}</td>
                        <td>{{(!empty($u->birthday)) ? date('d-m-Y', strtotime($u->birthday)) : 'Not updated'}}</td>
                        <td>{{$u->name}}</td>
                        <td>
                            <a href="{{ route('user_edit',['id'=>$u->id]) }}" class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-edit"></span></a>
                            <a href="{{ route('user_delete',['id'=>$u->id]) }}" class="btn btn-sm btn-danger" onclick="return confirm('Bạn có chắc muốn xóa tài khoản này không ?')"><span class="glyphicon glyphicon-trash"></span></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @if($count < 1)
                <h3 style="text-align: center;">You do not have any manager inventory !!!</h3>
            @else
            <a href="{{ route('user_edit_multi_password') }}" class="btn btn-sm btn-success" style="margin-left: 87%">Update password</a>
            @endif
            <div class="pull-right">
                {{ $users->links('vendor.pagination.default') }}
            </div>
        </div>
    </div>
</div>
@endsection
@section('css')
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/dist/css/table.css') }}">
@stop

@section('js')
     <script>
        if($("#error").text() != ""){
            w2popup.open({
                title   : 'ERROR',
                body    : $("#error").text(),
                width: 450,
                height: 90,
            });
        }
    </script>
@stop