@extends('adminlte::page')
@section('title', 'Update Password')
@section('content_header')
    <h1 style="margin-left: 18%;margin-bottom: 1.8%;">Update Password</h1>
@stop

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <div id="error" style="display: none">{{session('success')}}</div>
                    <form method="POST" action="">
                        @csrf
                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">User <small>(*)</small></label>

                            <div class="col-md-6">
                                <select name="users[]" class="form-control" multiple="multiple">
                                    @foreach($users as $u)
                                        <option value="{{$u->id}}" {{!empty($u->email) ? : 'disabled'}}>{{$u->username}}</option>
                                    @endforeach
                                </select>
                                <div style="color: red;">
                                    @if($errors->has('users'))
                                        {{ $errors->first('users') }}
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0" style="margin-left: 22%;padding-top: 2% !important;">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-sm btn-primary">
                                    Update
                                </button>
                                <a href="{{ route('user') }}" class="btn btn-sm btn-warning" style="margin-left: 10%;">Back</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('css')
    <link rel="stylesheet" href="">
@stop

@section('js')
     <script>
        if($("#error").text() != ""){
            w2popup.open({
                title   : 'Success',
                body    : $("#error").text(),
                width: 450,
                height: 90,
            });
        }
        
    </script>
@stop