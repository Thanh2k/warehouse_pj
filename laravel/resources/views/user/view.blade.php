@extends('adminlte::page')
@section('title', 'Your infomation')

@section('content')
<style>
    td {
        padding: 7px;
    }
    h1{
        margin-bottom: 5%;
        margin-top: 4%;
        margin-left: 3%;
        color: #53bba3;
    }
    .infor-user{
        margin-left: 19%;
    }
</style>
<div class="container infor-user">
    <div class="row">
        <div class="col-md-5">
            <div id="error" style="display: none">{{session('success')}}</div>
        </div>
    </div>
    <h1><span class="glyphicon glyphicon-user" style="margin-right: 1%;"></span>{{(!empty($model->full_name)) ? $model->full_name : 'Inventory management'}}</h1>

   <table>
        <tr>
            <th></th>
            <th></th>
        </tr>
        <tr>
            <td><strong>Email </strong></td>
            <td>: {{$model->email}}</td>
        </tr>
        <tr>
            <td><strong>Address </strong></td>
            <td>: {{(!empty($model->address)) ? $model->address : 'Not updated'}}</td>
        </tr>
        <tr>
            <td><strong>Phone </strong></td>
            <td>: {{(!empty($model->phone_number)) ? $model->phone_number : 'Not updated'}}</td>
        </tr>
        <tr>
            <td><strong>Birthday </strong></td>
            <td>: {{(!empty($model->birthday)) ? date('d-m-Y', strtotime($model->birthday)) : 'Not updated'}}</td>
        </tr>
    </table>
    <div style="margin-left: 25%;margin-top: 3%;">
        <a href="{{ route('user_edit_profile',['id' => $model->id]) }}" class="btn btn-sm btn-warning">Update Infor</a>
        <a href="{{ route('user_edit_password',['id' => $model->id]) }}" class="btn btn-sm btn-primary">Change password</a>
    </div>
</div>
@endsection
@section('css')
    <link rel="stylesheet" href="">
@stop

@section('js')
     <script>
        if($("#error").text() != ""){
            w2popup.open({
                title   : 'ERROR',
                body    : $("#error").text(),
                width: 450,
                height: 90,
            });
        }
        
    </script>
@stop