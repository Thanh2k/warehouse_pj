@extends('adminlte::page')
@section('title', 'Change Your Password')
@section('content_header')
    <h1 style="margin-left: 18%;margin-bottom: 1.8%;">Change Your Password</h1>
@stop

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <div id="error" style="display: none">{{session('error')}}</div>
                    <form method="POST" action="">
                        @csrf
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Old password <small>(*)</small></label>

                            <div class="col-md-6">
                                <input id="name" type="password" class="form-control" name="old_password" value="{{old('old_password')}}">
                                <div style="color: red;">
                                    @if($errors->has('old_password'))
                                        {{ $errors->first('old_password') }}
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">New password<small>(*)</small></label>

                            <div class="col-md-6">
                                <input id="name" type="password" class="form-control" name="password" value="{{ old('password') }}">
                                <div style="color: red;">
                                    @if($errors->has('password'))
                                        {{ $errors->first('password') }}
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Confirm password<small>(*)</small></label>

                            <div class="col-md-6">
                                <input id="name" type="password" class="form-control" name="confirm_password" value="{{ old('confirm_password') }}">
                                <div style="color: red;">
                                    @if($errors->has('confirm_password'))
                                        {{ $errors->first('confirm_password') }}
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0" style="margin-left: 27%;padding-top: 2% !important;">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Update
                                </button>
                                <a href="{{ route('user_view') }}" class="btn btn-warning" style="margin-left: 4%;">Back</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('css')
    <link rel="stylesheet" href="">
@stop

@section('js')
     <script>
        if($("#error").text() != ""){
            w2popup.open({
                title   : 'ERROR',
                body    : $("#error").text(),
                width: 450,
                height: 90,
            });
        }
        
    </script>
@stop