@extends('adminlte::page')
@section('title', 'Register')
@section('content_header')
    <h1 style="margin-left: 14%;color: #047354;padding-bottom: 1.5%;">Register Manager Inventory</h1>
@stop

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{ route('user_add') }}">
                        @csrf
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Username <small>(*)</small></label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="username" autocomplete="off" value="{{ old('username') }}">
                                <div style="color: red;">
                                    @if($errors->has('username'))
                                        {{ $errors->first('username') }}
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">Password <small>(*)</small></label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" autocomplete="off" name="password" value="{{ old('password') }}">
                                <div style="color: red;">
                                    @if($errors->has('password'))
                                        {{ $errors->first('password') }}
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }} <small>(*)</small></label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="confirm_password" value="{{ old('confirm_password') }}">
                                <div style="color: red;">
                                    @if($errors->has('confirm_password'))
                                        {{ $errors->first('confirm_password') }}
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">Role<small>(*)</small></label>
                            <div class="col-md-6 offset-md-4">
                                <select name="roles" class="form-control" size="4">
                                    @foreach($roles as $r)
                                        <option value="{{$r->id}}" @if (old('roles') == $r->id) {{ 'selected' }} @endif>{{$r->name}}</option>
                                    @endforeach
                                </select>
                                <div style="color: red;">
                                    @if($errors->has('roles'))
                                        {{ $errors->first('roles') }}
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0" style="margin-left: 10%;padding-top: 2% !important;">
                            <div class="col-md-6 offset-md-4" style="margin-left: 15%;">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('css')
    <link rel="stylesheet" href="">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop