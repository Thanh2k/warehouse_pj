@extends('adminlte::page')
@section('title', 'Edit Inventory Manager')
@section('content_header')
    <h1 style="margin-left: 20%;color: #49498e;padding-bottom: 1.5%;">Edit User {{$model->username}}</h1>
@stop

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <div id="error" style="display: none">{{session('error')}}</div>
                    <form method="POST" action="">
                        @csrf
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Username <small>(*)</small></label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="username" value="{{$model->username}}">
                                <div style="color: red;">
                                    @if($errors->has('username'))
                                        {{ $errors->first('username') }}
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }} <small>(*)</small></label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" value="{{ old('password') }}">
                                <div style="color: red;">
                                    @if($errors->has('password'))
                                        {{ $errors->first('password') }}
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }} <small>(*)</small></label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="confirm_password" value="{{ old('confirm_password') }}">
                                <div style="color: red;">
                                    @if($errors->has('confirm_password'))
                                        {{ $errors->first('confirm_password') }}
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">Role <small>(*)</small></label>

                            <div class="col-md-6">
                                <select name="roles" size="4" class="form-control">
                                    @foreach($roles as $r)
                                        <option {{ $role->contains($r->id) ? 'selected' : ''}}
                                        value="{{$r->id}}">{{$r->name}}</option>
                
                                    @endforeach
                                </select>
                                <div style="color: red;">
                                    @if($errors->has('roles'))
                                        {{ $errors->first('roles') }}
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0" style="margin-left: 14%;padding-top: 2% !important;">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Update
                                </button>
                                <a href="{{ route('user') }}" class="btn btn-warning" style="margin-left: 5%;">Back</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('css')
    <link rel="stylesheet" href="">
@stop

@section('js')
     <script>
        if($("#error").text() != ""){
            w2popup.open({
                title   : 'ERROR',
                body    : $("#error").text(),
                width: 450,
                height: 90,
            });
        }
        
    </script>
@stop