@extends('adminlte::page')
@section('title', 'Update Your Information')
@section('content_header')
    <h1 style="margin-left: 15%;color: #49498e;padding-bottom: 1.5%;">Update Your Information</h1>
@stop

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <div id="error" style="display: none">{{session('error')}}</div>
                    <form method="POST" action="">
                        @csrf
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Full name <small>(*)</small></label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="full_name" value="{{$user->full_name}}">
                                <div style="color: red;">
                                    @if($errors->has('full_name'))
                                        {{ $errors->first('full_name') }}
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Email <small>(*)</small></label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="email" value="{{$user->email}}">
                                <div style="color: red;">
                                    @if($errors->has('email'))
                                        {{ $errors->first('email') }}
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Address</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" max="200" name="address" value="{{$user->address}}">
                                <div style="color: red;">
                                    @if($errors->has('address'))
                                        {{ $errors->first('address') }}
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Phone</label>

                            <div class="col-md-6">
                                <input id="name" type="number" class="form-control" name="phone_number" value="{{$user->phone_number}}">
                                <div style="color: red;">
                                    @if($errors->has('phone_number'))
                                        {{ $errors->first('phone_number') }}
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Birthday</label>

                            <div class="col-md-6">
                                <input id="name" type="date" class="form-control" name="birthday" value="{{$user->birthday}}">
                                <div style="color: red;">
                                    @if($errors->has('birthday'))
                                        {{ $errors->first('birthday') }}
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0" style="margin-left: 20%;padding-top: 2% !important;">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-sm btn-primary">
                                    Update
                                </button>
                                <a href="{{ route('user_view') }}" class="btn btn-sm btn-warning" style="margin-left: 13%;">Back</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('css')
    <link rel="stylesheet" href="">
@stop

@section('js')
     <script>
        if($("#error").text() != ""){
            w2popup.open({
                title   : 'ERROR',
                body    : $("#error").text(),
                width: 450,
                height: 90,
            });
        }
    </script>
@stop