<!DOCTYPE html>
<html lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Title Page</title>

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	</head>
	<body>
		<table class="table table-bordered table-hover">
			<thead>
				<tr>
					<th colspan="5" style="text-align: center;font-weight: bold;">Alibaba Import And Export Company</th>
				</tr>
				<tr>
					<th colspan="5" style="text-align: center;font-weight: bold;">{{$inventory->address}}</th>
				</tr>
				<tr></tr>
				<tr>
					<th colspan="13" style="font-weight: bold;font-size: 18px;text-align: center;">Product List Inventory {{$inventory->name}}</th>
				</tr>
				<tr>
					<th colspan="13" style="font-weight: bold;text-align: center;">{{$date}}</th>
				</tr>
				<tr></tr>
				<tr>
					<th style="text-align: center;"><b>STT</b></th>
					<th colspan="2" style="text-align: center;"><b>Product name</b></th>
					<th colspan="2" style="text-align: center;"><b>Category name</b></th>
					<th colspan="2" style="text-align: center;"><b>Unit name</b></th>
					<th colspan="2" style="text-align: center;"><b>Supplier name</b></th>
					<th colspan="2" style="text-align: center;"><b>Real quantity</b></th>
					<th colspan="2" style="text-align: center;"><b>Product price</b></th>
				</tr>
			</thead>
			<tbody>
				<?php $stt = 1; $total = 0; $totalPrice = 0; ?>
				@foreach($products as $p)
				<tr>
					<td style="text-align: center;">{{$stt++}}</td>
					<td colspan="2" style="text-align: center;">{{$p->name}}</td>
					<td colspan="2" style="text-align: center;">{{$p->catname}}</td>
					<td colspan="2" style="text-align: center;">{{$p->unitname}}</td>
					<td colspan="2" style="text-align: center;">{{$p->supname}}</td>
					<td colspan="2" style="text-align: center;">{{$p->real_quantity}}</td>
					<td colspan="2" style="text-align: center;"><?php echo number_format($p->newest_price_import,2); ?> $</td>
				</tr>
				<?php 
					$total += $p->real_quantity; 
					$totalPrice += ($p->real_quantity * $p->newest_price_import); 
				?>
				@endforeach
				<tr></tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td colspan="2" style="text-align: center;"><b>Total Product</b></td>
					<td colspan="2" style="text-align: center;"><b>Total Price</b></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td colspan="2" style="text-align: center;">{{$total}}</td>
					<td colspan="2" style="text-align: center;"><?php echo number_format($totalPrice,2); ?> $</td>
				</tr>
			</tbody>
		</table>

		<script src="https://code.jquery.com/jquery.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	</body>
</html>