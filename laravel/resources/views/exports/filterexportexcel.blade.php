<!DOCTYPE html>
<html lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Title Page</title>

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	</head>
	<body>
		<table class="table table-bordered table-hover">
			<thead>
				<tr>
					<th colspan="5" style="text-align: center;font-weight: bold;">Alibaba Import And Export Company</th>
				</tr>
				<tr>
					<th colspan="5" style="text-align: center;font-weight: bold;">{{$inventory->address}}</th>
				</tr>
				<tr></tr>
				<tr>
					<th colspan="15" style="font-weight: bold;font-size: 18px;text-align: center;">Export {{$inventory->name}} Report</th>
				</tr>
				<tr></tr>
				<tr></tr>
				<tr>
					<th style="text-align: center;"><b>STT</b></th>
					<th colspan="2" style="text-align: center;"><b>Product name</b></th>
					<th colspan="2" style="text-align: center;"><b>Customer</b></th>
					<th colspan="2" style="text-align: center;"><b>User export</b></th>
					<th colspan="2" style="text-align: center;"><b>Quantity export</b></th>
					<th colspan="2" style="text-align: center;"><b>Price export</b></th>
					<th colspan="2" style="text-align: center;"><b>Total</b></th>
					<th colspan="2" style="text-align: center;"><b>Export Date</b></th>
				</tr>
			</thead>
			<tbody>
				<?php $stt = 1; $total = 0; $totalPrice = 0; ?>
				@foreach($export as $e)
				<tr>
					<td style="text-align: center;">{{$stt++}}</td>
					<td colspan="2" style="text-align: center;">{{$e->proname}}</td>
					<td colspan="2" style="text-align: center;">{{$e->cusname}}</td>
					<td colspan="2" style="text-align: center;">{{$e->username}}</td>
					<td colspan="2" style="text-align: center;">{{$e->quantity_export}}</td>
					<td colspan="2" style="text-align: center;"><?php echo number_format($e->price_export,2); ?> $</td>
					<td colspan="2" style="text-align: center;"><?php echo number_format(($e->price_export * $e->quantity_export),2); ?> $</td>
					<td colspan="2" style="text-align: center;">{{date('d-m-Y', strtotime($e->created_at))}}</td>
				</tr>
				<?php 
					$total += $e->quantity_export; 
					$totalPrice += ($e->price_export * $e->quantity_export); 
				?>
				@endforeach
				<tr></tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td colspan="2" style="text-align: center;"><b>Total Product</b></td>
					<td colspan="2" style="text-align: center;"><b>Total Price</b></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td colspan="2" style="text-align: center;">{{$total}}</td>
					<td colspan="2" style="text-align: center;"><?php echo number_format($totalPrice,2); ?> $</td>
				</tr>
				<tr></tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td colspan="3" style="font-weight: bold;text-align: center;">..... ,{{$date}}</td>
				</tr>
				<tr></tr>
				<tr>
					<td></td>
					<td></td>
					<td colspan="3" style="text-align: center;font-weight: bold;">Inventory Manager</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td colspan="3" style="text-align: center;font-weight: bold;">Approver</td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td colspan="3" style="text-align: center;">(signature)</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td colspan="3" style="text-align: center;">(signature)</td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td colspan="3"></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td colspan="3"></td>
				</tr>
			</tbody>
		</table>

		<script src="https://code.jquery.com/jquery.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	</body>
</html>