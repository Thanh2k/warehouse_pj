<!DOCTYPE html>
<html lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Title Page</title>

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	</head>
	<body>
		<table class="table table-bordered table-hover">
			<thead>
				<tr>
					<th colspan="5" style="text-align: center;font-weight: bold;">Alibaba Import And Export Company</th>
				</tr>
				<tr>
					<th colspan="5" style="text-align: center;font-weight: bold;">{{$inventory->address}}</th>
				</tr>
				<tr></tr>
				<tr>
					<th colspan="17" style="font-weight: bold;font-size: 18px;text-align: center;">Import {{$inventory->name}} Report</th>
				</tr>
				<tr></tr>
				<tr></tr>
				<tr>
					<th style="text-align: center;"><b>STT</b></th>
					<th colspan="2" style="text-align: center;"><b>Product name</b></th>
					<th colspan="2" style="text-align: center;"><b>User import</b></th>
					<th colspan="2" style="text-align: center;"><b>Supplier</b></th>
					<th colspan="2" style="text-align: center;"><b>Type</b></th>
					<th colspan="2" style="text-align: center;"><b>Quantity import</b></th>
					<th colspan="2" style="text-align: center;"><b>Price import</b></th>
					<th colspan="2" style="text-align: center;"><b>Total</b></th>
					<th colspan="2" style="text-align: center;"><b>Import Date</b></th>
				</tr>
			</thead>
			<tbody>
				<?php $stt = 1; $total = 0; $totalPrice = 0; ?>
				@foreach($import as $i)
				<tr>
					<td style="text-align: center;">{{$stt++}}</td>
					<td colspan="2" style="text-align: center;">{{$i->proname}}</td>
					<td colspan="2" style="text-align: center;">{{$i->username}}</td>
					<td colspan="2" style="text-align: center;">{{$i->supname}}</td>
					<td colspan="2" style="text-align: center;">{{($i->type == 1) ? 'Returned product' : 'New product'}}</td>
					<td colspan="2" style="text-align: center;">{{$i->quantity_import}}</td>
					<td colspan="2" style="text-align: center;">
						@if($i->type == 1) 
                            <?php echo number_format($i->price_product_return,2); ?> $
                        @else
                            <?php echo number_format($i->price_import,2); ?> $
                        @endif
					</td>
					<td colspan="2" style="text-align: center;">
						@if($i->type == 1) 
                            <?php echo number_format(($i->price_product_return * $i->quantity_import),2); ?> $
                        @else
                            <?php echo number_format(($i->price_import * $i->quantity_import),2); ?> $
                        @endif
					</td>
					<td colspan="2" style="text-align: center;">{{date('d-m-Y', strtotime($i->created_at))}}</td>
				</tr>
				<?php 
					$total += $i->quantity_import; 
					if($i->type == 1) {
						$totalPrice += ($i->price_product_return * $i->quantity_import); 
					}else{
						$totalPrice += ($i->price_import * $i->quantity_import); 
					}
				?>
				@endforeach
				<tr></tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td colspan="2" style="text-align: center;"><b>Total Product</b></td>
					<td colspan="2" style="text-align: center;"><b>Total Price</b></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td colspan="2" style="text-align: center;">{{$total}}</td>
					<td colspan="2" style="text-align: center;"><?php echo number_format($totalPrice,2); ?> $</td>
				</tr>
				<tr></tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td colspan="4" style="font-weight: bold;text-align: center;">..... ,{{$date}}</td>
				</tr>
				<tr></tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td colspan="3" style="text-align: center;font-weight: bold;">Inventory Manager</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td colspan="3" style="text-align: center;font-weight: bold;">Approver</td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td colspan="3" style="text-align: center;">(signature)</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td colspan="3" style="text-align: center;">(signature)</td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td colspan="3"></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td colspan="3"></td>
				</tr>
			</tbody>
		</table>

		<script src="https://code.jquery.com/jquery.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	</body>
</html>