<!DOCTYPE html>
<html lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Title Page</title>

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	</head>
	<body>
		<table class="table table-bordered table-hover">
			<thead>
				<tr>
					<th colspan="7" style="text-align: center;font-weight: bold;font-size: 20px;">Import Product</th>
				</tr>
				<tr></tr>
				<tr>
					<th style="text-align: center;"><b>STT</b></th>
					<th style="text-align: center;"><b>Product name</b></th>
					<th style="text-align: center;"><b>Category name</b></th>
					<th style="text-align: center;"><b>Unit </b></th>
					<th style="text-align: center;"><b>Supplier </b></th>
					<th style="text-align: center;"><b>Quantity import</b></th>
					<th style="text-align: center;"><b>Price import</b></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			</tbody>
		</table>

		<script src="https://code.jquery.com/jquery.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	</body>
</html>