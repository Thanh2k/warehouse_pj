@extends('adminlte::page')
@section('title', 'Top Product Returned')
@section('content_header')
<h1 style="margin-left: 30%;color: red;">Top Product Returned Inventory {{$inventory->name}}</h1>
@stop

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Stt</th>
                        <th>Product name</th>
                        <th>Total Quantity Product returned</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $stt = 1; ?>
                    @foreach($import as $i)
                      <tr>
                          <td style="width: 25%;">{{$stt++}}</td>
                          <td style="width: 33%;">{{$i->name}}</td>
                          <td style="width: 33%;">{{$i->sumimport}}</td>
                      </tr>
                   @endforeach
               </tbody>
           </table>
           <a href="{{route('product_returned',['invenid'=>$invenid])}}" class="btn btn-sm btn-success" style="margin-left: 92%;">Back</a>
       </div>
    </div>
</div>
@endsection
@section('css')
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/dist/css/table.css') }}">
@stop

@section('js')
<script>
    
</script>
@stop