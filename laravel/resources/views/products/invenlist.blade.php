@extends('adminlte::page')
@section('title', 'Inventory List')
@section('content_header')
<h1 style="margin-left: 38%;color: red;">Inventory List</h1>
@stop

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Stt</th>
                        <th>Product name</th>
                        <th>Quantity</th>
                        <th>Import date</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $stt = 1; $flag = false; ?>
                    @foreach($invenList as $iL)
                    <?php $a = $dateNow->diffInDays(date('d-m-Y', strtotime($iL->dateimport)),false); ?>
                        @if($a <= (-365) && $iL->real_quantity > 0)
                        <?php $flag = true; ?>
                            <tr>
                                <td>{{$stt++}}</td>
                                <td><a style="color: #423d3d;" href="{{ route('view_product',['invenid'=>$invenid,'proid'=>$iL->proid]) }}">{{$iL->proname}}</a></td>
                                <td>{{$iL->real_quantity}}</td>
                                <td>{{date('d-m-Y', strtotime($iL->dateimport))}}</td>
                            </tr>
                        @endif
                   @endforeach
               </tbody>
            </table>
            <div class="text-center">
                @if($flag == false)
                    <h3>You do not have any product !</h3>
                @endif
            </div>
           <a href="{{ route('product',['invenid'=>$invenid]) }}" class="btn btn-sm btn-warning" style="margin-left: 90%;">Back</a>
       </div>
    </div>
</div>
@endsection
@section('css')
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/dist/css/table.css') }}">
@stop

@section('js')
<script></script>
@stop