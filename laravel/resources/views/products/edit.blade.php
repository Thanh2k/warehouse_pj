@extends('adminlte::page')
@section('title', 'Edit Products')
@section('content_header')
    <h1 style="margin-left: 15%;color: #49498e;padding-bottom: 1%;">Edit Product {{$product->name}}</h1>
@stop

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <div id="error" style="display: none">{{session('error')}}</div>
                    <form method="POST" action="{{ route('product_edit', ['id' => $product->id, 'invenid' => $invenid]) }}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Product name <small>(*)</small></label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{$product->name}}">
                                <div style="color: red;">
                                    @if($errors->has('name'))
                                        {{ $errors->first('name') }}
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">Category <small>(*)</small></label>

                            <div class="col-md-6">
                                <select name="category_id" class="form-control">
                                    <option value="0">----- Categories -----</option>
                                    <?php Helpers::showCategoriesForEditProduct($categories,$product->category_id); ?>
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Suppliers <small>(*)</small></label>

                            <div class="col-md-6">
                                <select name="supplier_id" class="form-control">
                                    @foreach($suppliers as $s)
                                        <option value="{{$s->id}}" {{( $product->supid == $s->id) ? 'selected' : ''}}>{{$s->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Units <small>(*)</small></label>

                            <div class="col-md-6">
                                <select name="unit_id" class="form-control">
                                    @foreach($units as $u)
                                        <option value="{{$u->id}}" {{( $product->unitid == $u->id) ? 'selected' : ''}}>{{$u->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Image</label>

                            <div class="col-md-6">
                                <input id="name" type="file" name="image" value="">
                                <div style="color: red;">
                                    @if($errors->has('image'))
                                        {{ $errors->first('image') }}
                                    @endif
                                </div>
                            </div>
                        </div>
                        @if($product->image)
                        <img src="/img/product/{{ $product->image }}" style="margin-left: 35%;margin-bottom: 2%;" height="90px" width="100px" />
                        @endif

                        <div class="form-group row mb-0" style="margin-left: 20%;padding-top: 2% !important;">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Update
                                </button>
                                <a href="{{ route('product',['invenid'=>$invenid]) }}" class="btn btn-warning" style="margin-left: 11%;">Back</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('css')
    <link rel="stylesheet" href="">
@stop

@section('js')
     <script>
        if($("#error").text() != ""){
            w2popup.open({
                title   : 'ERROR',
                body    : $("#error").text(),
                width: 450,
                height: 90,
            });
        }
        
    </script>
@stop