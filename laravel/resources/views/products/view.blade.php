@extends('adminlte::page')
@section('title', 'Your infomation')

@section('content')
<style>
    td {
        padding: 7px;
    }
    h1{
        margin-bottom: 4%;
        margin-top: 4%;
        margin-left: 5%;
        color: #5d4015;
        font-size: 293%;
    }
    .infor-pro{
        margin-left: 19%;
    }
    .image{
        margin-top: 8.5%;
    }
    .info {
        width: 34.666667%;
    }
</style>
<div class="container infor-pro">
    <div class="row">
        <div class="col-md-5 info">
            <div id="error" style="display: none">{{session('success')}}</div>
            <h1>Product : {{$invenProduct->name}}</h1>

            <table>
                <tr>
                    <th></th>
                    <th></th>
                </tr>
                <tr>
                    <td><strong>Category </strong></td>
                    <td>: {{$invenProduct->catname}}</td>
                </tr>
                <tr>
                    <td><strong>Supplier</strong></td>
                    <td>: {{$invenProduct->supname}}</td>
                </tr>
                <tr>
                    <td><strong>Import quantity</strong></td>
                    <td>: {{$invenProduct->sumimport}}</td>
                </tr>
                <tr>
                    <td><strong>Export quantity</strong></td>
                    <td>: {{($invenProduct->sumimport - $invenProduct->real_quantity)}}</td>
                </tr>
                <tr>
                    <td><strong>Real quantity</strong></td>
                    <td>: {{$invenProduct->real_quantity}}</td>
                </tr>
                <tr>
                    <td><strong>Price import</strong></td>
                    <td>: <?php echo ($invenProduct->real_quantity == 0) ? '0' : number_format($invenProduct->newest_price_import,2); ?> $</td>
                </tr>
                <tr>
                    <td><strong>Unit </strong></td>
                    <td>: {{$invenProduct->unitname}}</td>
                </tr>
                <tr>
                    <td><strong>Created at</strong></td>
                    <td>: {{date('d-m-Y', strtotime($invenProduct->created_at))}}</td>
                </tr>
            </table>
        </div>
        <div class="col-md-7 image">
            @if($invenProduct->image)
                <img src="/img/product/{{ $invenProduct->image }}" height="155px" width="230px" />
            @endif
        </div>
    </div>
    
    <div style="margin-left: 20%;margin-top: 3%;">
        <a href="{{ route('product_edit',['id'=>$idpro,'invenid'=>$invenid]) }}" class="btn btn-sm btn-warning">Edit Product</a>
        <a href="{{ route('product',['invenid'=>$invenid]) }}" class="btn btn-sm btn-success" style="margin-left: 1%;">Back</a>
    </div>
</div>
@endsection
@section('css')
    <link rel="stylesheet" href="">
@stop

@section('js')
     <script>
        if($("#error").text() != ""){
            w2popup.open({
                title   : 'ERROR',
                body    : $("#error").text(),
                width: 450,
                height: 90,
            });
        }
        
    </script>
@stop