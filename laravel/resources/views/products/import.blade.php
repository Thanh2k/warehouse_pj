@extends('adminlte::page')
@section('title', 'Import Product')
@section('content_header')
    <h1 style="margin-left: 15%;color: #49498e;padding-bottom: 1%;">Import Product</h1>
@stop

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <div id="error" style="display: none">{{session('error')}}</div>
                    <form method="POST" action="{{ route('product_import', ['invenid'=>$invenid]) }}" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Product name <small>(*)</small></label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">
                                <div style="color: red;">
                                    @if($errors->has('name'))
                                        {{ $errors->first('name') }}
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label id="sup" class="col-md-4 col-form-label text-md-right">Category <small>(*)</small></label>

                            <div class="col-md-6">
                                <select name="category_id" class="form-control" required>
                                    <option value="">----- Category -----</option>
                                    <?php Helpers::showCategoriesForAddView($categories); ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label id="sup" class="col-md-4 col-form-label text-md-right">Supplier <small>(*)</small></label>

                            <div class="col-md-6">
                                <select name="supplier_id" class="form-control" required>
                                    <option value="">----- Suppliers -----</option>
                                    @foreach($supplier as $s)
                                        <option value="{{$s->id}}" @if (old('supplier_id') == $s->id) {{ 'selected' }} @endif>{{$s->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label id="sup" class="col-md-4 col-form-label text-md-right">Unit <small>(*)</small></label>

                            <div class="col-md-6">
                                <select name="unit_id" class="form-control" required>
                                    <option value="">----- Units -----</option>
                                    @foreach($units as $u)
                                        <option value="{{$u->id}}" @if (old('unit_id') == $u->id) {{ 'selected' }} @endif>{{$u->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Image</label>

                            <div class="col-md-6">
                                <input id="name" type="file" name="image" value="">
                                <div style="color: red;">
                                    @if($errors->has('image'))
                                        {{ $errors->first('image') }}
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Quantity import <small>(*)</small></label>

                            <div class="col-md-6">
                                <input id="name" type="number" class="form-control" name="quantity_import" value="{{ old('quantity_import') }}">
                                <div style="color: red;">
                                    @if($errors->has('quantity_import'))
                                        {{ $errors->first('quantity_import') }}
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Price import <small>(*)</small></label>

                            <div class="col-md-6">
                                <input id="name" type="number" class="form-control" step="0.01" name="price_import" value="{{ old('price_import') }}">
                                <div style="color: red;">
                                    @if($errors->has('price_import'))
                                        {{ $errors->first('price_import') }}
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0" style="margin-left: 19%;padding-top: 2% !important;">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Import
                                </button>
                                <a href="{{ route('product',['invenid'=>$invenid]) }}" class="btn btn-warning" style="margin-left: 9%;">Back</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('css')
    <link rel="stylesheet" href="">
@stop
@section('js')
     <script>
        if($("#error").text() != ""){
            w2popup.open({
                title   : 'ERROR',
                body    : $("#error").text(),
                width: 450,
                height: 90,
            });
        }
        
    </script>
@stop
