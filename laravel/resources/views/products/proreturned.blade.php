@extends('adminlte::page')
@section('title', 'Product returned List')
@section('content_header')
<h1 style="margin-left: 33%;color: #49498e;">Product Returned List</h1>
@stop

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div id="error" style="display: none">{{session('error')}}</div>
        <div class="col-md-10">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Stt</th>
                        <th>Product name</th>
                        <th>Supplier</th>
                        <th>Customer return</th>
                        <th>Reason return</th>
                        <th>Quantity returned</th>
                        <th>Price returned</th>
                        <th>Returned date</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php $stt = 1; ?>
                    @foreach($proReturned as $pR)
                    <tr>
                        <td>{{$stt++}}</td>
                        <td>{{$pR->name}}</td>
                        <td>{{$pR->supname}}</td>
                        @foreach($customers as $c)
                            @if($c->id == $pR->customer_return)
                                <td>{{$c->name}}</td>
                            @endif
                        @endforeach
                        <td style="width: 22%;">{{ $pR->reason_return }}</td>
                        <td style="width: 8%;">{{$pR->quantity_import}}</td>
                        <td><?php echo number_format($pR->price_product_return,2); ?> $</td>
                        <td>{{date('d-m-Y', strtotime($pR->created_at))}}</td> 
                        <td>
                            <a href="{{ route('pro_returned_edit',['invenid'=>$invenid,'proid'=>$pR->product_id,'importid'=>$pR->id]) }}" class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-edit"></span></a>
                            <a href="{{ route('pro_returned_delete',['invenid'=>$invenid,'proid'=>$pR->product_id,'importid'=>$pR->id]) }}" class="btn btn-sm btn-danger" onclick="return confirm('Are you sure ?')"><span class="glyphicon glyphicon-trash"></span></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @if($count < 1)
                <h3 style="text-align: center;">Inventory do not have any product returned !!!</h3>
                <a href="{{ route('pro_returned_add',['invenid'=>$invenid]) }}" class="{{($inventory->max == $inventory->total) ? 'disabled ' : ''}}btn btn-sm btn-info" style="margin-left: 78%;">Add Product Returned</a>
                <a href="{{ route('invendetail') }}" class="btn btn-sm btn-warning">Back</a>
            @else
                <a href="{{ route('top_pro_returned',['invenid'=>$invenid]) }}" class="btn btn-sm btn-success" style="margin-left: 65%;">Top Product Returned</a>
                <a href="{{ route('pro_returned_add',['invenid'=>$invenid]) }}" class="btn btn-sm btn-info">Add Product Returned</a>
                <a href="{{ route('invendetail') }}" class="btn btn-sm btn-warning">Back</a>
            @endif
       </div>
    </div>
</div>
@endsection
@section('css')
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/dist/css/table.css') }}">
@stop

@section('js')
    <script>
        if($("#error").text() != ""){
            w2popup.open({
                title   : 'ERROR',
                body    : $("#error").text(),
                width: 450,
                height: 90,
            });
        }
        
    </script>
@stop