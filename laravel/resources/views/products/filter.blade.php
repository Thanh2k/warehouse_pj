@extends('adminlte::page')
@section('title', 'Products')
@section('content_header')
<h1 style="margin-left: 32%;color: red;">Products Inventory {{$inventory->name}}</h1>
<h4 style="margin-left: 39%">Find: {{$countResult}} product(s)</h4>
@stop

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="col-md-6" style="margin-bottom: 2%;margin-left: 76%;">
                <form class="form-inline" action="{{ route('filter_product') }}" method="GET">
                    <div>
                        <input class="form-control" style="width: 165px;" name="key" type="text" placeholder="Search" aria-label="Search">
                        <input type="hidden" name="invenid" value="{{$invenid}}">
                        <button type="submit" class="btn btn-primary"><i class="fas fa-search" aria-hidden="true"></i></button>
                    </div>
                </form>
            </div>
            <table class="table table-hover" style="margin-top: 1%;">
                <thead>
                    <tr>
                        <th>Stt</th>
                        <th>Product name</th>
                        <th>Product image</th>
                        <th>Category</th>
                        <th>Supplier</th>
                        <th>Real quantity</th>
                        <th>Import price</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php $stt = 1; ?>
                        @foreach($invenProduct as $i)
                            <tr>
                                <td>{{$stt++}}</td>
                                <td>{{$i->name}}</td>
                                <td>
                                    @if($i->image)
                                    <img src="/img/product/{{ $i->image }}" height="55px" width="68px" />
                                    @endif
                                </td>
                                <td>
                                    <a href="{{ route('categories') }}" style="color: #423d3d;">{{$i->catname}}</a>
                                </td>
                                <td>
                                    <a href="{{ route('unit') }}" style="color: #423d3d;">{{$i->unitname}}</a>
                                </td>
                                <td style="width: 6%;">
                                        {{$i->real_quantity}}
                                </td>
                                <td style="width: 10%;">
                                    <?php echo ($i->real_quantity == 0) ? '0' : number_format($i->newest_price_import,2); ?> $
                                </td>
                                <td style="width: 19%;">
                                    <a href="{{ route('view_product',['invenid'=>$invenid,'proid'=>$i->product_id]) }}" class="btn btn-sm btn-primary"><i class="fa fa-eye"></i></a>
                                    <a href="{{ route('product_import_plus',['invenid'=>$invenid,'proid'=>$i->product_id]) }}" class="{{($inventory->max == $inventory->total) ? 'disabled ' : ''}}btn btn-sm btn-success"><span class="glyphicon glyphicon-import"></span></a>
                                    <a href="{{ route('product_export',['invenid'=>$invenid,'proid'=>$i->product_id]) }}" class="{{($i->real_quantity == 0) ? 'disabled ' : ''}}btn btn-sm btn-info"><span class="glyphicon glyphicon-export"></span></a>
                                    <a href="{{ route('product_delete',['id'=>$i->product_id,'invenid'=>$invenid]) }}" class="btn btn-sm btn-danger" onclick="return confirm('If you delete this product, you may lose the system history. Are you sure ?')"><span class="glyphicon glyphicon-trash"></span></a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <div style="margin-left: 65%;">
                    <a href="{{ route('import_by_excel',['invenid' => $invenid]) }}" class="{{($inventory->max == $inventory->total) ? 'disabled ' : ''}}btn btn-sm btn-primary excel">Import data by excel file</a>
                    <a href="{{ route('product_import', ['invenid'=>$invenid]) }}" class="btn btn-sm btn-success">Import new product</a>
                    <a href="{{ route('product',['invenid'=>$invenid]) }}" class="btn btn-sm btn-warning">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('css')
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/dist/css/table.css') }}">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop