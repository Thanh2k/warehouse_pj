@extends('adminlte::page')
@section('title', 'Product Returned')
@section('content_header')
    <h1 style="margin-left: 15%;color: #49498e;padding-bottom: 1%;">Product Returned</h1>
@stop

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <div id="error" style="display: none">{{session('error')}}</div>
                    <form method="POST" action="{{ route('pro_returned_add', ['invenid'=>$invenid]) }}" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Product name <small>(*)</small></label>

                            <div class="col-md-6">
                                <select name="product_id" class="form-control">
                                    @foreach($products as $p)
                                        <option value="{{$p->id}}" @if (old('product_id') == $p->id) {{ 'selected' }} @endif>{{$p->name}}</option>
                                    @endforeach
                                </select>
                                <div style="color: red;">
                                    @if($errors->has('product_id'))
                                        {{ $errors->first('product_id') }}
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Supplier of the above product <small>(*)</small></label>

                            <div class="col-md-6">
                                <select name="supplier_id" class="form-control">
                                    @foreach($suppliers as $s)
                                        <option value="{{$s->id}}" @if (old('supplier_id') == $s->id) {{ 'selected' }} @endif>{{$s->name}}</option>
                                    @endforeach
                                </select>
                                <div style="color: red;">
                                    @if($errors->has('supplier_id'))
                                        {{ $errors->first('supplier_id') }}
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Customer return <small>(*)</small></label>

                            <div class="col-md-6">
                                <select name="customer_return" class="form-control">
                                    @foreach($customers as $c)
                                        <option value="{{$c->id}}" @if (old('customer_return') == $c->id) {{ 'selected' }} @endif>{{$c->name}}</option>
                                    @endforeach
                                </select>
                                <div style="color: red;">
                                    @if($errors->has('customer_return'))
                                        {{ $errors->first('customer_return') }}
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Reason return <small>(*)</small></label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" name="reason_return" value="{{ old('reason_return') }}">
                                <div style="color: red;">
                                    @if($errors->has('reason_return'))
                                        {{ $errors->first('reason_return') }}
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Quantity return <small>(*)</small></label>

                            <div class="col-md-6">
                                <input id="name" type="number" class="form-control" name="quantity_import" value="{{ old('quantity_import') }}">
                                <div style="color: red;">
                                    @if($errors->has('quantity_import'))
                                        {{ $errors->first('quantity_import') }}
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Price return <small>(*)</small></label>

                            <div class="col-md-6">
                                <input type="number" class="form-control" step="0.01" name="price_import" value="{{ old('price_import') }}">
                                <div style="color: red;">
                                    @if($errors->has('price_import'))
                                        {{ $errors->first('price_import') }}
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0" style="margin-left: 19%;padding-top: 2% !important;">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Import
                                </button>
                                <a href="{{ route('product_returned',['invenid'=>$invenid]) }}" class="btn btn-warning" style="margin-left: 9%;">Back</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('css')
    <link rel="stylesheet" href="">
@stop
@section('js')
     <script>
        if($("#error").text() != ""){
            w2popup.open({
                title   : 'ERROR',
                body    : $("#error").text(),
                width: 450,
                height: 90,
            });
        }
        
    </script>
@stop
