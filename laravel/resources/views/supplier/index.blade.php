@extends('adminlte::page')
@section('title', 'Suppliers')
@section('content_header')
    <h1 style="margin-left: 35%;color: #49498e;">Suppliers</h1>
@stop
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
           <div id="error" style="display: none">{{session('error')}}</div>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Stt</th>
                        <th style="width: 32%;">Supplier name</th>
                        <th style="width: 28%;">Supplier address</th>
                        <th>Supplier phone</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php $stt = 1; ?>
                    @foreach($suppliers as $s)
                    <tr>
                        <td>{{$stt++}}</td>
                        <td>{{$s->name}}</td>
                        <td>{{(!empty($s->address)) ? $s->address : 'Not updated'}}</td>
                        <td>{{(!empty($s->phone_number)) ? $s->phone_number : 'Not updated'}}</td>
                        <td>
                            <a href="{{ route('supplier_edit',['id'=>$s->id]) }}" class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-edit"></span></a>
                            <a href="{{ route('supplier_delete',['id'=>$s->id]) }}" class="btn btn-sm btn-danger" onclick="return confirm('If you delete this supplier, you may lose the system history. Are you sure ?')"><span class="glyphicon glyphicon-trash"></span></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @if($count < 1)
                <h3 style="text-align: center;">You do not have any supplier !!!</h3>
            @endif
            <div style="margin-left: 89%;">
               <a href="{{ route('supplier_add') }}" class="btn btn-sm btn-success">Add supplier</a>
            </div>
            <div class="pull-right">
                {{ $suppliers->links('vendor.pagination.default') }}
            </div>
        </div>
    </div>
</div>
@endsection
@section('css')
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/dist/css/table.css') }}">
@stop

@section('js')
    <script>
        if($("#error").text() != ""){
            w2popup.open({
                title   : 'ERROR',
                body    : $("#error").text(),
                width: 450,
                height: 90,
            });
        }
        
    </script>
@stop
