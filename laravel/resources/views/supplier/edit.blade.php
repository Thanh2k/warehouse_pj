@extends('adminlte::page')
@section('title', 'Edit Supplier')
@section('content_header')
    <h1 style="margin-left: 10%;color: #49498e;padding-bottom: 1.5%;">Edit Supplier {{$supplier->name}}</h1>
@stop

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <div id="error" style="display: none">{{session('error')}}</div>
                    <form method="POST" action="">
                        @csrf
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Supplier Name <small>(*)</small></label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{$supplier->name}}">
                                <div style="color: red;">
                                    @if($errors->has('name'))
                                        {{ $errors->first('name') }}
                                    @endif
                                </div>
                            </div>
                        </div>

                       <div class="form-group row">
                            <label for="" class="col-md-4 col-form-label text-md-right">Supplier address <small>(*)</small></label>

                            <div class="col-md-6">
                                <input id="" type="text" class="form-control" name="address" value="{{$supplier->address}}">
                                <div style="color: red;">
                                    @if($errors->has('address'))
                                        {{ $errors->first('address') }}
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">Supplier phone <small>(*)</small></label>

                            <div class="col-md-6">
                                <input id="" type="text" class="form-control" name="phone_number" value="{{$supplier->phone_number}}">
                                <div style="color: red;">
                                    @if($errors->has('phone_number'))
                                        {{ $errors->first('phone_number') }}
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0" style="margin-left: 17%;padding-top: 2% !important;">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-sm btn-primary">
                                    Update
                                </button>
                                <a href="{{ route('supplier') }}" class="btn btn-sm btn-warning" style="margin-left: 8%;">Back</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('css')
    <link rel="stylesheet" href="">
@stop

@section('js')
     <script>
        if($("#error").text() != ""){
            w2popup.open({
                title   : 'ERROR',
                body    : $("#error").text(),
                width: 450,
                height: 90,
            });
        }
        
    </script>
@stop