@extends('adminlte::page')
@section('title', 'Inventories')
@section('content_header')
    <h1 style="margin-left: 35%; color: #49498e;">Inventories</h1>
@stop
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Stt</th>
                        <th>Inventory name</th>
                        <th>Inventory address</th>
                        <th>Maximum capacity</th>
                        <th>Containing</th>
                        <th>The rest</th>
                        <th>Inventory manager</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php $stt = 1; ?>
                    @foreach($inventories as $i)
                    <tr>
                        <td>{{$stt++}}</td>
                        <td style="width: 22%;">{{$i->name}}</td>
                        <td style="width: 22%;">{{$i->address}}</td>
                        <td style="width: 9%;">{{$i->max}}</td>
                        <td>{{$i->total}}</td>
                        <td>{{$i->the_rest}}</td>
                        <td>
                            <?php foreach ($userInventory as $ui): ?>
                                <?php if ($ui->inventory_id == $i->id): ?>
                                    <?php foreach ($users as $u): ?>
                                      <?php if ($u->id == $ui->user_id): ?>
                                        {{$u->username}}
                                        <br>
                                        <?php endif ?>
                                    <?php endforeach ?>
                                <?php endif ?>
                            <?php endforeach ?>
                        </td>
                        <td>
                            <a href="{{ route('inventory_edit',['id'=>$i->id]) }}" class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-edit"></span></a>
                            <button onclick="deleteInventory(<?php echo $i->id; ?>)" class="btn btn-sm btn-danger"><span class="glyphicon glyphicon-trash"></span></button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @if($count < 1)
                <h3 style="text-align: center;">You do not have any inventory !!!</h3>
            @endif
            <div style="margin-left: 90%;">
               <a href="{{ route('inventory_add') }}" class="btn btn-sm btn-success">Add inventory</a>
            </div>
            <div class="pull-right">
                {{ $inventories->links('vendor.pagination.default') }}
            </div>
        </div>
    </div>
</div>
@endsection
@section('css')
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/dist/css/table.css') }}">
@stop
@section('js')
<script>
    function deleteInventory(id){
        var check = confirm('If you delete this inventory, you may lose the system history. Are you sure ?');
        if(check){
            $.ajax({
                url: "inventory/delete-" + id,
                type: "GET",
                data: {},
                success: function(response){
                    $("section.content").html(response);
                }
            });
        }
    }
</script>
@stop