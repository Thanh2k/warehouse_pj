@extends('adminlte::page')
@section('title', 'Edit Inventory')
@section('content_header')
    <h1 style="margin-left: 15%;color: #49498e;padding-bottom: 1%;">Edit Inventory {{$inventory->name}}</h1>
@stop

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <div id="error" style="display: none">{{session('error')}}</div>
                    <form method="POST" action="">
                        @csrf
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }} <small>(*)</small></label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{$inventory->name}}">
                                <div style="color: red;">
                                    @if($errors->has('name'))
                                        {{ $errors->first('name') }}
                                    @endif
                                </div>
                            </div>
                        </div>

                       <div class="form-group row">
                            <label for="" class="col-md-4 col-form-label text-md-right">Inventory address <small>(*)</small></label>

                            <div class="col-md-6">
                                <input id="" type="text" class="form-control" name="address" value="{{$inventory->address}}">
                                <div style="color: red;">
                                    @if($errors->has('address'))
                                        {{ $errors->first('address') }}
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="" class="col-md-4 col-form-label text-md-right">Maximum capacity <small>(*)</small></label>

                            <div class="col-md-6">
                                <input id="" type="text" class="form-control" name="max" value="{{$inventory->max}}">
                                <div style="color: red;">
                                    @if($errors->has('max'))
                                        {{ $errors->first('max') }}
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">Inventory Manager <small>(*)</small></label>

                            <div class="col-md-6">
                                <select name="managers[]" class="form-control" multiple="multiple">
                                    @foreach($users as $u)
                                        <option {{ $listManager->contains($u->id) ? 'selected' : ''}}
                                        value="{{$u->id}}">{{$u->username}}</option>
                
                                    @endforeach
                                </select>
                                <div style="color: red;">
                                    @if($errors->has('managers'))
                                        {{ $errors->first('managers') }}
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0" style="margin-left: 18%;padding-top: 2% !important;">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-sm btn-primary">
                                    Update
                                </button>
                                <a href="{{ route('inventory') }}" class="btn btn-sm btn-warning" style="margin-left: 9%;">Back</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('css')
    <link rel="stylesheet" href="">
@stop

@section('js')
     <script>
        if($("#error").text() != ""){
            w2popup.open({
                title   : 'ERROR',
                body    : $("#error").text(),
                width: 450,
                height: 90,
            });
        }
        
    </script>
@stop