@extends('adminlte::page')
@section('title', 'Categories')
@section('content_header')
    <h1 style="margin-left: 37%;color: #49498e;">Categories</h1>
@stop
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div id="error" style="display: none">{{session('error')}}</div>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th style="width: 17%;">Id</th>
                        <th>Name</th>
                        <th style="width: 35%;"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php Helpers::showCategoriesForIndexView($categories) ?>
                </tbody>
            </table>
            @if($count < 1)
                <h3 style="text-align: center;">You do not have any category !!!</h3>
            @endif
            <div style="margin-left: 85%;">
                 <a href="{{ route('categories_add') }}" class="btn btn-sm btn-success">Add categories</a>
            </div>
        </div>
    </div>
</div>
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/dist/css/table.css') }}">
@stop

@section('js')
    <script>
        if($("#error").text() != ""){
            w2popup.open({
                title   : 'ERROR',
                body    : $("#error").text(),
                width: 420,
                height: 80,
            });
        }
    </script>
@stop
