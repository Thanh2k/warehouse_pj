@extends('adminlte::page')
@section('title', 'Update Category')
@section('content_header')
    <h1 style="margin-left: 15%;color: #49498e;padding-bottom: 1%;">Update Category {{$category->name}}</h1>
@stop

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                   <div id="error" style="display: none">{{session('error')}}</div>
                    <form method="POST" action="">
                        @csrf
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Category<small>(*)</small></label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{$category->name}}">
                                <div style="color: red;">
                                    @if($errors->has('name'))
                                        {{ $errors->first('name') }}
                                    @endif
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">Parent category</label>
                            <div class="col-md-6">
                                <select name="parent_id" class="form-control">
                                    <option value="0">--- Parent Category ---</option>
                                    <?php
                                        foreach ($categories as $value) {
                                            if($value->parent_id == $category->parent_id) {
                                                Helpers::clean($categories, $value->id);
                                            }
                                        }
                                        Helpers::showCategoriesForEditView($categories, $category);
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row mb-0" style="margin-left: 18%;padding-top: 2% !important;">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-sm btn-primary">
                                    Update
                                </button>
                                <a href="{{ route('categories') }}" class="btn btn-sm btn-warning" style="margin-left: 8%;">Back</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('css')
    <link rel="stylesheet" href="">
@stop

@section('js')
    <script>
        if($("#error").text() != ""){
            w2popup.open({
                title   : 'ERROR',
                body    : $("#error").text(),
                width: 450,
                height: 90,
            });
        }
        
    </script>
@stop