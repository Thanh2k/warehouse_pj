@extends('adminlte::page')

@section('title', 'Inventory Management')

@section('content_header')
@stop

@section('content')
    <div class="list-group-item list-group-item-info inventory animated fadeInUp"><h2><a href="{{route('inventory')}}">Inventories</a></h2> <i class="fa fa-database"></i><strong>{{$countInven}}</strong></div>
    <div class="list-group-item list-group-item-success manager animated fadeInDown"><h2><a href="{{route('user')}}">Managers</a></h2> <i class="fas fa-street-view"></i><strong>{{$countManager}}</strong></div>
    <div class="list-group-item list-group-item-warning customer animated fadeInUp"><h2><a href="{{route('customer')}}">Customers</a></h2> <i class="fa fa-users"></i><strong>{{$countCus}}</strong></div>
    <div class="list-group-item list-group-item-danger supplier animated fadeInDown"><h2><a href="{{route('supplier')}}">Suppliers</a></h2> <i class="far fa-building"></i><strong>{{$countSup}}</strong></div>
    <div class="row">
        <div class="col-md-6 animated bounceInRight" style="margin-top: 2%;margin-left: 76%;">
            <form action="{{ route('filter_home') }}" method="GET">
                <div>
                    <input class="form-control" style="width: 120px;" name="key" type="number" placeholder="Year" aria-label="Year" autocomplete="off">
                </div>
            </form>
        </div>
        <div class="col-md-11" style="margin-left: 2.5%;margin-top: 2%;">
            {!! $chart->container() !!}
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/dist/css/style.css') }}">
    <style>
        .form-control{
            border-top: none;
            border-left: none;
            border-right: none;
            border-bottom: #00f6ff solid 1px;
        }
    </style>
@stop
@section('js')
    {!! $chart->script() !!}
    <script>
        $("#datepicker").datetimepicker({
            format:'Y',
            timepicker:false,
            pick:null,
        });
        
        function chartHome (key) {
            $.ajax({
                url: "filter?key=" + key,
                type: "GET",
                data: {},
                success:function (res) {
                    $("section.content").html(res);
                }
            });
        }
    </script>
@stop