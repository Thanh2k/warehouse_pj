<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Login và Logout
Route::get('/', 'Admin\AdminController@login')->name('login');
Route::group(['namespace' => 'Admin'],function ()
{
	Route::get('/login', 'AdminController@login')->name('login');
	Route::post('/login', 'AdminController@postLogin')->name('login');
	Route::get('/forgot-password', 'AdminController@forgotPass')->name('forgot_pass');
	Route::post('/forgot-password', 'AdminController@postForgotPass')->name('forgot_pass');
	Route::get('/logout', 'AdminController@logout')->name('logout');
});

// Middleware check việc đăng nhập
Route::middleware(['checklogin'])->group(function () {

	//Route thay đổi mật khẩu khi lần đầu đăng nhập
	Route::group(['namespace' => 'Admin'],function ()
	{
		Route::get('/change','ChangeinfoController@change')->name('changeinfo');
		Route::post('/change','ChangeinfoController@postChange')->name('changeinfo');
	});

	//Middeware check việc đăng nhập lần đầu tiên
	Route::middleware(['checkfirstlogin'])->group(function () {
		Route::get('/home', 'HomeController@index')->name('home');
		Route::get('/home-filter','HomeController@getFilter')->name('filter_home');

		// User 
		Route::group(['prefix' => 'user','namespace' => 'Admin'],function ()
		{
			Route::get('/','UserController@index')->name('user');
			Route::get('add','UserController@add')->name('user_add');
			Route::post('add','UserController@postAdd')->name('user_add');
			Route::get('view','UserController@view')->name('user_view');
			Route::get('editprofile-{id}','UserController@editProfile')->name('user_edit_profile');
			Route::post('editprofile-{id}','UserController@postEditProfile')->name('user_edit_profile');
			Route::get('editpassword-{id}','UserController@editPassword')->name('user_edit_password');
			Route::post('editpassword-{id}','UserController@postEditPassword')->name('user_edit_password');
			Route::get('updatemultipleuser','UserController@editMultiPassword')->name('user_edit_multi_password');
			Route::post('updatemultipleuser','UserController@postEditMultiPassword')->name('user_edit_multi_password');
			Route::get('delete-{id}','UserController@delete')->name('user_delete');
			//Middleware check quyền admin
			Route::middleware(['checkadmin:Admin'])->group(function () {
				Route::get('edit-{id}','UserController@edit')->name('user_edit');
				Route::post('edit-{id}','UserController@postEdit')->name('user_edit');
			});
		});

		// Category
		Route::group(['prefix' => 'categories','namespace' => 'Admin'],function ()
		{
			Route::get('/','CategoryController@index')->name('categories');
			Route::get('add','CategoryController@add')->name('categories_add');
			Route::post('add','CategoryController@postAdd')->name('categories_add');
			Route::get('edit-{id}','CategoryController@edit')->name('categories_edit');
			Route::post('edit-{id}','CategoryController@postEdit')->name('categories_edit');
			Route::get('delete-{id}','CategoryController@delete')->name('categories_delete');
		});

		// Units
		Route::group(['prefix' => 'units','namespace' => 'Admin'],function ()
		{
			Route::get('/','UnitController@index')->name('unit');
			Route::get('add','UnitController@add')->name('unit_add');
			Route::post('add','UnitController@postAdd')->name('unit_add');
			Route::get('edit-{id}','UnitController@edit')->name('unit_edit');
			Route::post('edit-{id}','UnitController@postEdit')->name('unit_edit');
			Route::get('delete-{id}','UnitController@delete')->name('unit_delete');
		});

		// Inventory
		Route::group(['prefix' => 'inventory','namespace' => 'Admin'],function ()
		{
			Route::get('/','InventoryController@index')->name('inventory');
			Route::get('add','InventoryController@add')->name('inventory_add');
			Route::post('add','InventoryController@postAdd')->name('inventory_add');
			Route::get('delete-{id}','InventoryController@delete')->name('inventory_delete');
			Route::middleware(['checkadmin:Admin'])->group(function () {
				Route::get('edit-{id}','InventoryController@edit')->name('inventory_edit');
				Route::post('edit-{id}','InventoryController@postEdit')->name('inventory_edit');
			});
		});

		//Supplier
		Route::group(['prefix' => 'supplier','namespace' => 'Admin'],function ()
		{
			Route::get('/','SupplierController@index')->name('supplier');
			Route::get('add','SupplierController@add')->name('supplier_add');
			Route::post('add','SupplierController@postAdd')->name('supplier_add');
			Route::get('edit-{id}','SupplierController@edit')->name('supplier_edit');
			Route::post('edit-{id}','SupplierController@postEdit')->name('supplier_edit');
			Route::get('delete-{id}','SupplierController@delete')->name('supplier_delete');
		});

		//Customer 
		Route::group(['prefix' => 'customer','namespace' => 'Admin'],function ()
		{
			Route::get('/','CustomerController@index')->name('customer');
			Route::get('add','CustomerController@add')->name('customer_add');
			Route::post('add','CustomerController@postAdd')->name('customer_add');
			Route::get('edit-{id}','CustomerController@edit')->name('customer_edit');
			Route::post('edit-{id}','CustomerController@postEdit')->name('customer_edit');
			Route::get('delete-{id}','CustomerController@delete')->name('customer_delete');
			Route::get('filter','CustomerController@getFilter')->name('filter_customer');
		});

		//Products
		Route::group(['prefix' => 'product','namespace' => 'Admin'],function ()
		{
			Route::get('/-{invenid}','ProductController@index')->name('product');
			Route::get('product-excel-{invenid}','ProductController@excelProduct')->name('product_excel');
			Route::get('template-import-data','ProductController@templateImport')->name('template_import');
			Route::get('product-returned-{invenid}','ProductReturnedController@productReturned')->name('product_returned');
			Route::get('add-pro-returned-{invenid}','ProductReturnedController@addProReturned')->name('pro_returned_add');
			Route::post('add-pro-returned-{invenid}','ProductReturnedController@postAddProReturned')->name('pro_returned_add');
			Route::get('edit-pro-returned-{invenid}-{proid}-{importid}','ProductReturnedController@editProReturned')->name('pro_returned_edit');
			Route::post('edit-pro-returned-{invenid}-{proid}-{importid}','ProductReturnedController@postEditProReturned')->name('pro_returned_edit');
			Route::get('delete-pro-returned-{invenid}-{proid}-{importid}','ProductReturnedController@deleteProReturned')->name('pro_returned_delete');
			Route::get('top-pro-returned-{invenid}','ProductReturnedController@getTopProReturned')->name('top_pro_returned');
			Route::get('import-{invenid}','ProductController@import')->name('product_import');
			Route::post('import-{invenid}','ProductController@postImport')->name('product_import');
			Route::get('importplus-{invenid}-{proid}','ProductController@importPlus')->name('product_import_plus');
			Route::post('importplus-{invenid}-{proid}','ProductController@postImportPlus')->name('product_import_plus');
			Route::get('export-{invenid}-{proid}','ProductController@export')->name('product_export');
			Route::post('export-{invenid}-{proid}','ProductController@postExport')->name('product_export');
			Route::get('viewproduct-{invenid}-{proid}','ProductController@viewProduct')->name('view_product');
			Route::get('edit-{id}-{invenid}','ProductController@edit')->name('product_edit');
			Route::post('edit-{id}-{invenid}','ProductController@postEdit')->name('product_edit');
			Route::get('delete-{id}-{invenid}','ProductController@delete')->name('product_delete');
			Route::get('filter','ProductController@getFilter')->name('filter_product');
			Route::get('inventory-list-{invenid}','ProductController@inventoryList')->name('inven_list');
			Route::get('importexcel-{invenid}','ProductController@importByExcelFile')->name('import_by_excel');
			Route::post('importexcel-{invenid}','ProductController@postImportByExcelFile')->name('import_by_excel');
		});

		//Inventory Detail
		Route::group(['prefix' => 'invendetail','namespace' => 'Admin'],function ()
		{
			Route::get('/','InventoryDetailController@index')->name('invendetail');
			Route::get('import-detail-{invenid}','InventoryDetailController@importDetail')->name('importdetail');
			Route::get('excel-import-{invenid}','InventoryDetailController@importExcel')->name('import_excel');
			Route::get('export-detail-{invenid}','InventoryDetailController@exportDetail')->name('exportdetail');
			Route::get('list-pro-discharge-{invenid}','InventoryDetailController@dischargeGoods')->name('discharge_goods');
			Route::get('excel-export-{invenid}','InventoryDetailController@exportExcel')->name('export_excel');
			Route::get('chart-{invenid}','InventoryDetailController@chart')->name('chart');
			Route::get('filterex','InventoryDetailController@filterExport')->name('filterEx');
			Route::get('filterim','InventoryDetailController@filterImport')->name('filterIm');
			Route::get('excelfilterexport/{invenid}/{key}/{from}/{to}','InventoryDetailController@exportFilterExcel')->name('export_filter_excel');
			Route::get('excelfilterimport/{invenid}/{key}/{from}/{to}','InventoryDetailController@importFilterExcel')->name('import_filter_excel');
			Route::get('topselling-{invenid}','InventoryDetailController@getTopSelling')->name('top_selling');
			Route::get('editexport-{invenid}-{proid}-{exportid}','InventoryDetailController@exportEdit')->name('export_edit');
			Route::post('editexport-{invenid}-{proid}-{exportid}','InventoryDetailController@postExportEdit')->name('export_edit');
			Route::get('editimport-{invenid}-{proid}-{importid}','InventoryDetailController@importEdit')->name('import_edit');
			Route::post('editimport-{invenid}-{proid}-{importid}','InventoryDetailController@postImportEdit')->name('import_edit');
			Route::get('filter-chart-{invenid}','InventoryDetailController@getFilter')->name('filter_chart');
		});
	});
});

